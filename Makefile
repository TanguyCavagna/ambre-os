.PHONY: setup image qemu
.EXPORT_ALL_VARIABLES:

# Build userspace binaries
user-nasm:
	basename -s .s dsk/src/bin/*.s | xargs -I {} nasm dsk/src/bin/{}.s -o dsk/bin/{}.tmp
	basename -s .s dsk/src/bin/*.s | xargs -I {} sh -c "printf '\x7FBIN' | cat - dsk/bin/{}.tmp > dsk/bin/{}"
	rm dsk/bin/*.tmp
user-rust:
	basename -s .rs src/bin/*.rs | xargs -I {} touch dsk/bin/{}
	basename -s .rs src/bin/*.rs | xargs -I {} cargo rustc --release --bin {} -- -C relocation-model=static
	basename -s .rs src/bin/*.rs | xargs -I {} cp target/x86_64-rust-os/release/{} dsk/bin/{}
	#strip dsk/bin/*

bin = target/x86_64-rust-os/release/bootimage-rust_os.bin
img = disk.img

$(img):
	qemu-img create $(img) 32M

# Rebuild rust-os
image: $(img)
	cargo bootimage --no-default-features --release --bin rust_os
	dd conv=notrunc if=$(bin) of=$(img)

opts = -m 32 -nic model=rtl8139 -drive file=$(img),format=raw -audiodev driver=sdl,id=a0 -machine pcspk-audiodev=a0 -cpu host -accel kvm -display gtk

qemu:
	qemu-system-x86_64 $(opts)

test:
	cargo test --workspace --exclude "utils" --release -- -m 32 -device isa-debug-exit,iobase=0xf4,iosize=0x04 -serial stdio -display none

clean:
	cargo clean
