//! Kernel of the rust-os

#![no_std]
#![cfg_attr(test, no_main)]
#![feature(custom_test_frameworks)]
#![feature(alloc_error_handler)]
#![feature(abi_x86_interrupt)]
#![feature(const_mut_refs)]
#![test_runner(test_framework::test_runner::test_runner)]
#![reexport_test_harness_main = "test_main"]

pub mod api;
pub mod sys;
pub mod task;

extern crate alloc;

const KERNEL_SIZE: usize = 2 << 20; // 2 MB

#[cfg(test)]
use core::panic::PanicInfo;

#[cfg(not(test))]
use bootloader::BootInfo;
#[cfg(test)]
use bootloader::{entry_point, BootInfo};

//==========================
// ALLOC ERR
//==========================

#[alloc_error_handler]
fn alloc_error_handler(layout: alloc::alloc::Layout) -> ! {
    panic!("allocation error: {:?}", layout);
}

//==========================
// TESTS SETUP
//==========================

#[cfg(test)]
#[panic_handler]
pub fn panic_handler(info: &PanicInfo) -> ! { test_framework::test_panic_handler(info); }

#[cfg(test)]
entry_point!(test_kernel_main);

#[cfg(test)]
fn test_kernel_main(boot_info: &'static BootInfo) -> ! {
    init(boot_info);
    test_main();
    utils::hlt_loop();
}

//==========================
// KERNEL INIT
//==========================

pub fn init(boot_info: &'static BootInfo) {
    sys::vga::init();
    sys::gdt::init();
    sys::idt::init();
    sys::pic::init();
    sys::keyboard::init();
    sys::time::init();

    println!();
    log!(
        "RUST-OS - {} {}v{}{}\n",
        env!("CARGO_PKG_NAME"),
        sys::console::Style::color("Cyan"),
        env!("CARGO_PKG_VERSION"),
        sys::console::Style::reset()
    );
    println!();

    sys::mem::init(boot_info);
    sys::cpu::init();
    sys::pci::init();
    sys::net::init();
    sys::ata::init();
    sys::fs::init();
    sys::clock::init(); // requires memory
}
