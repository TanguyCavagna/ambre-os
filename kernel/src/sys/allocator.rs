use alloc::slice::SliceIndex;
use alloc::sync::Arc;
use alloc::vec;
use alloc::vec::Vec;
use core::{
    cmp,
    ops::{Index, IndexMut},
};

use linked_list_allocator::LockedHeap;
use spin::Mutex;
use x86_64::{
    structures::paging::{
        mapper::MapToError, page::PageRangeInclusive, FrameAllocator, Mapper, Page, PageTableFlags,
        Size4KiB,
    },
    VirtAddr,
};

use crate::sys;

//==========================
// STRUCT / IMPL
//==========================

#[derive(Clone)]
pub struct PhysBuf {
    buf: Arc<Mutex<Vec<u8>>>,
}

impl PhysBuf {
    /// Creates a new phyiscal buffer with the given lenght
    ///
    /// # Arguments
    ///
    /// * `len` - Length of the buffer
    pub fn new(len: usize) -> Self { Self::from(vec![0; len]) }

    /// Creates a buffer with contiunous physical memory chunk
    ///
    /// # Arguments
    ///
    /// * `vec` - Buffer to convert in physical buffer
    fn from(vec: Vec<u8>) -> Self {
        let buffer_len = vec.len() - 1;
        let memory_len = phys_addr(&vec[buffer_len]) - phys_addr(&vec[0]);
        if buffer_len == memory_len as usize {
            Self {
                buf: Arc::new(Mutex::new(vec)),
            }
        } else {
            Self::from(vec.clone()) // Not continuous, clone vec and try again
        }
    }

    /// Gets the phyiscal address of the buffer
    pub fn addr(&self) -> u64 { phys_addr(&self.buf.lock()[0]) }
}

/// Allows accessing values by indexes like vectors
impl<I: SliceIndex<[u8]>> Index<I> for PhysBuf {
    type Output = I::Output;

    /// Gets the requested index
    ///
    /// # Arguments
    ///
    /// * `index` - Index wanted
    fn index(&self, index: I) -> &Self::Output { Index::index(&**self, index) }
}

/// Allows accessing values by indexes for mutable types like vectors
impl<I: SliceIndex<[u8]>> IndexMut<I> for PhysBuf {
    /// Gets the requested index
    ///
    /// # Arguments
    ///
    /// * `index` - Index wanted
    fn index_mut(&mut self, index: I) -> &mut Self::Output {
        IndexMut::index_mut(&mut **self, index)
    }
}

impl core::ops::Deref for PhysBuf {
    type Target = [u8];

    /// Dereferences
    fn deref(&self) -> &[u8] {
        let vec = self.buf.lock();

        unsafe { alloc::slice::from_raw_parts(vec.as_ptr(), vec.len()) }
    }
}

impl core::ops::DerefMut for PhysBuf {
    /// Dereferences mutable
    fn deref_mut(&mut self) -> &mut [u8] {
        let mut vec = self.buf.lock();

        unsafe { alloc::slice::from_raw_parts_mut(vec.as_mut_ptr(), vec.len()) }
    }
}

//==========================
// GLOBAL ALLOCATOR
//==========================

#[global_allocator]
static ALLOCATOR: LockedHeap = LockedHeap::empty();

//==========================
// PUBLIC
//==========================

/// Gets the physical address of a given pointer
///
/// # Arguments
///
/// * `prt` - Pointer
pub fn phys_addr(ptr: *const u8) -> u64 {
    let virt_addr = VirtAddr::new(ptr as u64);
    let phys_addr = sys::mem::virt_to_phys(virt_addr).unwrap();

    phys_addr.as_u64()
}

/// Gets the memory size
pub fn memory_size() -> usize { ALLOCATOR.lock().size() }

/// Gets the memory used space
pub fn memory_used() -> usize { ALLOCATOR.lock().used() }

/// Gets the memory free space
pub fn memory_free() -> usize { ALLOCATOR.lock().free() }

/// Frees the given pages
///
/// # Arguments
///
/// * `addr` - Page address
/// * `size` - Size to free
// TODO: Replace `free` by `dealloc`
pub fn free_pages(addr: u64, size: usize) {
    let mut mapper = unsafe { sys::mem::mapper(VirtAddr::new(sys::mem::PHYS_MEM_OFFSET)) };
    let pages: PageRangeInclusive<Size4KiB> = {
        let start_page = Page::containing_address(VirtAddr::new(addr));
        let end_page = Page::containing_address(VirtAddr::new(addr + (size as u64) - 1));

        Page::range_inclusive(start_page, end_page)
    };

    for page in pages {
        if let Ok((_frame, mapping)) = mapper.unmap(page) {
            mapping.flush();
        } else {
            debug!("Could not unmap {:?}", page);
        }
    }
}

/// Allocs a page
///
/// # Arguments
///
/// * `addr` - Page address
/// * `size` - Size to allocate
pub fn alloc_pages(addr: u64, size: usize) {
    // debug!("Alloc pages (addr={:#x}, size={})", addr, size);
    let mut mapper = unsafe { sys::mem::mapper(VirtAddr::new(sys::mem::PHYS_MEM_OFFSET)) };
    let mut frame_allocator =
        unsafe { sys::mem::BootInfoFrameAllocator::init(sys::mem::MEMORY_MAP.unwrap()) };
    let flags =
        PageTableFlags::PRESENT | PageTableFlags::WRITABLE | PageTableFlags::USER_ACCESSIBLE;
    let pages = {
        let start_page = Page::containing_address(VirtAddr::new(addr));
        let end_page = Page::containing_address(VirtAddr::new(addr + (size as u64) - 1));

        Page::range_inclusive(start_page, end_page)
    };

    for page in pages {
        // debug!("Alloc page {:?}", page);
        if let Some(frame) = frame_allocator.allocate_frame() {
            unsafe {
                if let Ok(mapping) = mapper.map_to(page, frame, flags, &mut frame_allocator) {
                    mapping.flush();
                } else {
                    debug!("Could not map {:?}", page);
                }
            }
        } else {
            debug!("Could not allocate frame for {:?}", page);
        }
    }
}

//==========================
// HEAP
//==========================

pub const HEAP_START: usize = 0x_4444_4444_0000;
pub const HEAP_SIZE: u64 = 16 << 20; // 16 MB

/// Initialize the heap
pub fn init_heap(
    mapper: &mut impl Mapper<Size4KiB>,
    frame_allocator: &mut impl FrameAllocator<Size4KiB>,
) -> Result<(), MapToError<Size4KiB>> {
    if !cfg!(feature = "quiet") {
        log!("Initializing heap... ");
    }

    // cap the heap size to 16 MB because allocator is too slow
    // !!! for developping purposes, heap set to 1MB !!!
    let heap_size = cmp::min(sys::mem::memory_size() / 2, HEAP_SIZE);
    let heap_start = VirtAddr::new(HEAP_START as u64);

    // create the heap page range
    let page_range = {
        let heap_end = heap_start + heap_size - 1u64;
        let heap_start_page = Page::containing_address(heap_start);
        let heap_end_page = Page::containing_address(heap_end);

        Page::range_inclusive(heap_start_page, heap_end_page)
    };

    // set all the page's frame as present and writable
    let flags = PageTableFlags::PRESENT | PageTableFlags::WRITABLE;
    for page in page_range {
        let frame = frame_allocator.allocate_frame().ok_or(MapToError::FrameAllocationFailed)?;

        unsafe {
            mapper.map_to(page, frame, flags, frame_allocator)?.flush();
        };
    }

    unsafe {
        ALLOCATOR.lock().init(heap_start.as_mut_ptr(), heap_size as usize);
    }

    if !cfg!(feature = "quiet") {
        let csi_color = crate::sys::console::Style::color("LightGreen");
        let csi_reset = crate::sys::console::Style::reset();
        println!("{}[ok]{}", csi_color, csi_reset);
    }

    log!("HEAP {} MB\n", heap_size >> 20);

    Ok(())
}

//==========================
// TESTS
//==========================

#[cfg(test)]
mod tests {
    #[test_case]
    fn many_boxes() {
        use alloc::boxed::Box;

        let heap_value_1 = Box::new(42);
        let heap_value_2 = Box::new(1337);
        assert_eq!(*heap_value_1, 42);
        assert_eq!(*heap_value_2, 1337);

        for i in 0..1000 {
            let x = Box::new(i);
            assert_eq!(*x, i);
        }
    }

    #[test_case]
    fn large_vec() {
        use alloc::vec::Vec;

        let n = 1000;
        let mut vec = Vec::new();
        for i in 0..n {
            vec.push(i);
        }
        assert_eq!(vec.iter().sum::<u64>(), (n - 1) * n / 2);
    }
}
