use time::{Duration, OffsetDateTime};

use super::fs::FileIO;
use crate::sys;
use crate::sys::cmos::CMOS;

const DAYS_BEFORE_MONTH: [u64; 13] = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365];
const SECONDS_IN_A_DAY: u64 = 86400;
const SECONDS_IN_AN_HOUR: u64 = 3600;
const SECONDS_IN_A_MINUTE: u64 = 60;

//==========================
// STRUCT / IMPL
//==========================

#[derive(Debug, Clone)]
pub struct Uptime;

impl Uptime {
    pub fn new() -> Self { Self {} }
}

impl FileIO for Uptime {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, ()> {
        let time = uptime().to_be_bytes();
        let n = time.len();

        if buf.len() >= n {
            buf[0..n].clone_from_slice(&time);
            Ok(n)
        } else {
            Err(())
        }
    }

    fn write(&mut self, _buf: &[u8]) -> Result<usize, ()> {
        unimplemented!();
    }
}

#[derive(Debug, Clone)]
pub struct Realtime;

impl Realtime {
    pub fn new() -> Self { Self {} }
}

impl FileIO for Realtime {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, ()> {
        let time = realtime().to_be_bytes();
        let n = time.len();

        if buf.len() >= n {
            buf[0..n].clone_from_slice(&time);
            Ok(n)
        } else {
            Err(())
        }
    }

    fn write(&mut self, _buf: &[u8]) -> Result<usize, ()> {
        unimplemented!();
    }
}

//==========================
// PRIVATE
//==========================

/// Is the given year a leap year
///
/// # Arguments
///
/// * `year` - Year to check
fn is_leap_year(year: u64) -> bool { (year % 4 == 0) & ((year % 100 != 0) | (year % 400 == 0)) }

/// How many days are between 1970 and the given year
///
/// # Arguments
///
/// * `year` - Year to use
fn days_before_year(year: u64) -> u64 {
    (1970..year).fold(0, |days, y| days + if is_leap_year(y) { 366 } else { 365 })
}

/// How many days are between january 1970 and the given month of the given year
///
/// # Arguments
///
/// * `year` - Year to use
/// * `month` - Month to use
fn days_before_month(year: u64, month: u64) -> u64 {
    let leap_day = is_leap_year(year) && month > 2;
    DAYS_BEFORE_MONTH[(month as usize) - 1] + if leap_day { 1 } else { 0 }
}

//==========================
// PUBLIC
//==========================

/// Get the RTC value
pub fn realtime() -> f64 {
    let rtc = CMOS::new().rtc(); // assuming GMT

    let timestamp = SECONDS_IN_A_DAY * days_before_year(rtc.year as u64)
        + SECONDS_IN_A_DAY * days_before_month(rtc.year as u64, rtc.month as u64)
        + SECONDS_IN_A_DAY * (rtc.day - 1) as u64
        + SECONDS_IN_AN_HOUR * rtc.hour as u64
        + SECONDS_IN_A_MINUTE * rtc.minute as u64
        + rtc.second as u64;

    let fract = sys::time::time_between_ticks()
        * (sys::time::ticks() - sys::time::last_rtc_update()) as f64;

    debug!("FRACT: {}, TIMESTAMP: {}", fract, timestamp);

    (timestamp as f64) + fract
}

/// Gets the current uptime
pub fn uptime() -> f64 { sys::time::time_between_ticks() * sys::time::ticks() as f64 }

/// Get the amount of seconds to add depending on the UTC timezone.
///
/// # Arguments
///
/// * `zone` - UTC timezone in range -12..12 inclusives.
pub fn timezone(zone: i8) -> Duration { Duration::seconds(SECONDS_IN_AN_HOUR as i64 * zone as i64) }

pub fn init() {
    let s = realtime();
    let ns = Duration::nanoseconds(libm::floor(1e9 * (s - libm::floor(s))) as i64);
    let tz = timezone(2);
    let dt = OffsetDateTime::from_unix_timestamp(s as i64) + ns + tz;
    let rtc = dt.format("%F %H:%M:%S UTC");

    let tz_hours = tz.as_seconds_f32() as i32 / SECONDS_IN_AN_HOUR as i32;
    log!(
        "RTC {} ({}{:02}:00)\n",
        rtc,
        if tz_hours >= 0 { "+" } else { "" },
        tz_hours
    );
}

//==========================
// TESTS
//==========================
#[cfg(test)]
mod tests {
    use super::*;

    #[test_case]
    fn test_uptime() {
        assert!(uptime() > 0.0);
    }

    #[test_case]
    fn test_realtime() {
        assert!(realtime() > 1234567890.0);
    }
}
