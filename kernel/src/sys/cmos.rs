use core::hint::spin_loop;

use x86_64::instructions::interrupts;
use x86_64::instructions::port::Port;

const CMOS_UPDATE_IN_PROGRESS_BIT: u8 = 7;
const CMOS_ADDR: u16 = 0x70;
const CMOS_DATA: u16 = 0x71;
const CMOS_NMI_ENABLE_BIT: u8 = 0x7F;
const CMOS_NMI_DISABLE_BIT: u8 = 0x80;
const CMOS_UPDATE_ENDED: u8 = 0x04;
const CMOS_24_HOURS_FORMAT: u8 = 0x02;
const CMOS_HOUR_PM: u8 = 0x80;

//==========================
// ENUM / STRUCT / IMPL
//==========================

// see <https://wiki.osdev.org/CMOS#Getting_Current_Date_and_Time_from_RTC>
#[repr(u8)]
enum Register {
    Second = 0x00,
    Minute = 0x02,
    Hour   = 0x04,
    Day    = 0x07,
    Month  = 0x08,
    Year   = 0x09,
    // offset register to update RTC behaviour
    A      = 0x0A,
    B      = 0x0B,
    C      = 0x0C,
}

#[repr(u8)]
enum Interrupt {
    Periodic = 1 << 6,
    Alarm    = 1 << 5,
    Update   = 1 << 4,
}

#[derive(Debug, PartialEq)]
pub struct RTC {
    pub year: u16,
    pub month: u8,
    pub day: u8,
    pub hour: u8,
    pub minute: u8,
    pub second: u8,
}

pub struct CMOS {
    addr: Port<u8>,
    data: Port<u8>,
}

impl CMOS {
    /// Creates a new CMOS manager
    pub fn new() -> Self {
        // see <https://wiki.osdev.org/RTC#Setting_the_Registers>
        CMOS {
            addr: Port::new(CMOS_ADDR),
            data: Port::new(CMOS_DATA),
        }
    }

    /// Is the CMOS still updating
    fn is_updating(&mut self) -> bool {
        unsafe {
            self.addr.write(Register::A as u8);
            (self.data.read() >> CMOS_UPDATE_IN_PROGRESS_BIT) & 1 != 0
        }
    }

    /// Halt the CPU until the CMOS has updated
    fn wait_end_of_update(&mut self) {
        while self.is_updating() {
            spin_loop();
        }
    }

    /// Read the given CMOS's register
    ///
    /// # Arguments
    ///
    /// * `reg` - Register to read
    fn read_register(&mut self, reg: Register) -> u8 {
        unsafe {
            self.addr.write(reg as u8);
            self.data.read()
        }
    }

    /// Enable the Non Maskable Interrupt
    ///
    /// see <https://wiki.osdev.org/Non_Maskable_Interrupt#Usage>
    fn enable_nmi(&mut self) {
        unsafe {
            let prev = self.addr.read();
            self.addr.write(prev & CMOS_NMI_ENABLE_BIT);
        }
    }

    /// Disable the Non Maskable Interupt
    fn disable_nmi(&mut self) {
        unsafe {
            let prev = self.addr.read();
            self.addr.write(prev | CMOS_NMI_DISABLE_BIT);
        }
    }

    /// Notify the CMOS that an interrupt has ended
    pub fn notify_end_of_interrupt(&mut self) {
        unsafe {
            self.addr.write(Register::C as u8);
            self.data.read();
        }
    }

    /// Enable CMOS interrupt
    ///
    /// # Arguments
    ///
    /// * `interrupt` - Which interrupt to enable
    fn enable_interrupt(&mut self, interrupt: Interrupt) {
        interrupts::without_interrupts(|| {
            self.disable_nmi();
            unsafe {
                self.addr.write(Register::B as u8);
                let prev = self.data.read();
                self.addr.write(Register::B as u8); // make sure that we are still on register 0x0B
                self.data.write(prev | interrupt as u8);
            }
            self.enable_nmi();
            self.notify_end_of_interrupt();
        });
    }

    /// Set the CMOS interrupt as periodic
    ///
    /// # Arguments
    ///
    /// * `rate` - Interrupt rate between 3 and 15. Resulting in the frequency 32768 >> (rate - 1)
    pub fn set_periodic_interrupt(&mut self, rate: u8) {
        interrupts::without_interrupts(|| {
            self.disable_nmi();
            unsafe {
                self.addr.write(Register::A as u8);
                let prev = self.data.read();
                self.addr.write(Register::A as u8);
                self.data.write((prev & 0xF0) | rate);
            }
            self.enable_nmi();
            self.notify_end_of_interrupt();
        });
    }

    /// Enable periodic interrupt
    pub fn enable_periodic_interrupt(&mut self) { self.enable_interrupt(Interrupt::Periodic); }

    /// Enable alarm interrupt
    pub fn enable_alarm_interrupt(&mut self) { self.enable_interrupt(Interrupt::Alarm); }

    /// Enable update interrupt
    pub fn enable_update_interrupt(&mut self) { self.enable_interrupt(Interrupt::Update); }

    /// Convert any BCD values to its decimal form
    ///
    /// # Arguments
    ///
    /// * `bcd` - Value to convert
    fn bcd_to_dec(&mut self, bcd: u8) -> u8 { (bcd & 0x0F) + ((bcd / 16) * 10) }

    /// Read the RTC values
    ///
    /// # Returns
    ///
    /// RTC trait
    fn rtc_unchecked(&mut self) -> RTC {
        RTC {
            second: self.read_register(Register::Second),
            minute: self.read_register(Register::Minute),
            hour: self.read_register(Register::Hour),
            day: self.read_register(Register::Day),
            month: self.read_register(Register::Month),
            year: self.read_register(Register::Year) as u16,
        }
    }

    /// Read the RTC value with some verifications and formatting
    ///
    /// # Returns
    ///
    /// RTC trait
    pub fn rtc(&mut self) -> RTC {
        // Read twice the RTC, discard the result and try again if the reads
        // happened during an update
        let mut rtc;
        loop {
            self.wait_end_of_update();
            rtc = self.rtc_unchecked();
            self.wait_end_of_update();
            if rtc == self.rtc_unchecked() {
                break;
            }
        }

        let b = self.read_register(Register::B);

        // BCD Mode, see <https://embedded.fm/blog/2018/6/5/an-introduction-to-bcd>
        if b & CMOS_UPDATE_ENDED == 0 {
            rtc.second = self.bcd_to_dec(rtc.second);
            rtc.minute = self.bcd_to_dec(rtc.minute);
            // FIXME why not working with self.bcd_to_dec ???
            rtc.hour =
                ((rtc.hour & 0x0F) + (((rtc.hour & 0x70) / 16) * 10)) | (rtc.hour & CMOS_HOUR_PM); // highest bit set if pm, keep it if present
            rtc.day = self.bcd_to_dec(rtc.day);
            rtc.month = self.bcd_to_dec(rtc.month);
            rtc.year = self.bcd_to_dec(rtc.year as u8) as u16; // Get only the smallest 2 digits from the year
        }

        if (b & CMOS_24_HOURS_FORMAT == 0) && (rtc.hour & CMOS_HOUR_PM == 0) {
            rtc.hour = ((rtc.hour & 0x7F) + 12) % 24; // 0x7F discard all other values after the hour itself
        }

        rtc.year += 2000; // TODO: Don't forget to change this next century

        rtc
    }
}
