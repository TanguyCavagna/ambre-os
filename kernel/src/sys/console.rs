use alloc::string::{String, ToString};
use core::fmt;
use core::sync::atomic::{AtomicBool, Ordering};

use spin::Mutex;
use x86_64::instructions::interrupts;

use crate::sys;
use crate::sys::fs::file::FileIO;

pub static STDIN: Mutex<String> = Mutex::new(String::new());
pub static ECHO: AtomicBool = AtomicBool::new(true);
pub static RAW: AtomicBool = AtomicBool::new(false);

pub const ETX_KEY: char = '\x03'; // End of Text
pub const EOT_KEY: char = '\x04'; // End of Transmission
pub const BS_KEY: char = '\x08'; // Backspace
pub const ESC_KEY: char = '\x1B'; // Escape

//==========================
// IMPL
//==========================

#[derive(Debug, Clone)]
pub struct Console;

impl Console {
    pub fn new() -> Self { Self {} }
}

impl FileIO for Console {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, ()> {
        let mut str = if buf.len() == 4 {
            read_char().to_string()
        } else {
            read_line()
        };

        str.truncate(buf.len());

        let n = str.len();

        buf[0..n].copy_from_slice(str.as_bytes());

        Ok(n)
    }

    fn write(&mut self, buf: &[u8]) -> Result<usize, ()> {
        let str = String::from_utf8_lossy(buf);
        let n = str.len();
        println!("{}", str);

        Ok(n)
    }
}

#[derive(Clone, Copy)]
pub struct Style {
    foreground: Option<usize>,
    background: Option<usize>,
}

impl Style {
    /// Resets the style
    pub fn reset() -> Self {
        Self {
            foreground: None,
            background: None,
        }
    }

    /// Sets the foreground an discard the background
    pub fn foreground(name: &str) -> Self {
        Self {
            foreground: color_to_fg(name),
            background: None,
        }
    }

    /// Sets the foreground an keep the background
    pub fn with_foreground(self, name: &str) -> Self {
        Self {
            foreground: color_to_fg(name),
            background: self.background,
        }
    }

    /// Sets the background an discard the foreground
    pub fn background(name: &str) -> Self {
        Self {
            foreground: None,
            background: color_to_bg(name),
        }
    }

    /// Sets the background an keep the foreground
    pub fn with_background(self, name: &str) -> Self {
        Self {
            foreground: self.foreground,
            background: color_to_bg(name),
        }
    }

    /// Calls self::foreground
    pub fn color(name: &str) -> Self { Self::foreground(name) }

    /// Calls self::with_foreground
    pub fn with_color(self, name: &str) -> Self { self.with_foreground(name) }
}

impl fmt::Display for Style {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // prints the right escaping code depending on the colors
        if let Some(fg) = self.foreground {
            if let Some(bg) = self.background {
                write!(f, "\x1b[{};{}m", fg, bg)
            } else {
                write!(f, "\x1b[{}m", fg)
            }
        } else if let Some(bg) = self.background {
            write!(f, "\x1b[{}m", bg)
        } else {
            write!(f, "\x1b[0m")
        }
    }
}

//==========================
// PUBLIC
//==========================

/// Converts a color name to it's foreground color code
fn color_to_fg(name: &str) -> Option<usize> {
    match name {
        "Black" => Some(30),
        "Red" => Some(31),
        "Green" => Some(32),
        "Brown" => Some(33),
        "Blue" => Some(34),
        "Magenta" => Some(35),
        "Cyan" => Some(36),
        "LightGray" => Some(37),
        "DarkGray" => Some(90),
        "LightRed" => Some(91),
        "LightGreen" => Some(92),
        "Yellow" => Some(93),
        "LightBlue" => Some(94),
        "Pink" => Some(95),
        "LightCyan" => Some(96),
        "White" => Some(97),
        _ => None,
    }
}

/// Converts a color name to it's background color code
fn color_to_bg(name: &str) -> Option<usize> { color_to_fg(name).map(|fg| fg + 10) }

/// Gets console width
pub fn cols() -> usize { sys::vga::cols() }

/// Gets console height
pub fn rows() -> usize { sys::vga::rows() }

/// Disable echo
pub fn disable_echo() { ECHO.store(false, Ordering::SeqCst) }

/// Enable echo
pub fn enable_echo() { ECHO.store(true, Ordering::SeqCst) }

/// Is echo enabled
pub fn is_echo_enabled() -> bool { ECHO.load(Ordering::SeqCst) }

/// Disable raw
pub fn disable_raw() { RAW.store(false, Ordering::SeqCst); }

/// Enable raw
pub fn enable_raw() { RAW.store(true, Ordering::SeqCst); }

/// Is raw enabled
pub fn is_raw_enabled() -> bool { RAW.load(Ordering::SeqCst) }

/// Does the STDIN contains the ETX symbole
pub fn end_of_text() -> bool { interrupts::without_interrupts(|| STDIN.lock().contains(ETX_KEY)) }

/// Does the STDIN contains the EOT symbole
pub fn end_of_transmission() -> bool {
    interrupts::without_interrupts(|| STDIN.lock().contains(EOT_KEY))
}

/// Clear the STDIN
pub fn drain() {
    interrupts::without_interrupts(|| {
        STDIN.lock().clear();
    })
}

/// Reads a char from STDIN
pub fn read_char() -> char {
    // prevent some edges cases char handling behaviour
    disable_echo();
    enable_raw();

    // halt CPU until a char has been read
    loop {
        sys::time::halt();

        // read char
        let res = interrupts::without_interrupts(|| {
            let mut stdin = STDIN.lock();

            if !stdin.is_empty() {
                Some(stdin.remove(0))
            } else {
                None
            }
        });

        if let Some(c) = res {
            enable_echo();
            disable_raw();

            return c;
        }
    }
}

/// Reads a line from STDIN
pub fn read_line() -> String {
    // halt CPU until a line has been read
    loop {
        sys::time::halt();

        let res = interrupts::without_interrupts(|| {
            let mut stdin = STDIN.lock();

            match stdin.chars().next_back() {
                Some('\n') => {
                    let line = stdin.clone();
                    stdin.clear();

                    Some(line)
                }
                _ => None,
            }
        });

        if let Some(line) = res {
            return line;
        }
    }
}

/// Handles any key entered in STDIN
///
/// # Arguments
///
/// * `key` - Key added in STDIN
pub fn handle_key(key: char) {
    let mut stdin = STDIN.lock();

    if key == BS_KEY && !is_raw_enabled() {
        // avoid printing more backspaces than chars instered into STDIN
        if let Some(c) = stdin.pop() {
            if is_echo_enabled() {
                let n = match c {
                    ETX_KEY | EOT_KEY | ESC_KEY => 2, // char length of thoses keys is 2 bytes
                    _ => {
                        if (c as u32) < 0xFF {
                            1 // guard clause of only 1 byte long
                        } else {
                            c.len_utf8() // some UTF-8 chars have more than 1 bytes length
                        }
                    }
                };

                // insert the right amount of backspaces
                println!("{}", BS_KEY.to_string().repeat(n));
            }
        }
    } else {
        // cast key in the right type
        let key = if (key as u32) < 0xFF {
            (key as u8) as char
        } else {
            key
        };

        stdin.push(key);

        // handle special keys
        if is_echo_enabled() {
            match key {
                ETX_KEY => print!("^C"),
                EOT_KEY => print!("^D"),
                ESC_KEY => print!("^["),
                _ => print!("{}", key),
            }
        }
    }
}
