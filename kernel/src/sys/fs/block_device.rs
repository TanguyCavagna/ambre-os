use alloc::vec;
use alloc::vec::Vec;

use spin::Mutex;

use super::super_block::SuperBlock;
use crate::sys;
use crate::sys::fs::bitmap_block::BitmapBlock;
use crate::sys::fs::dir::Dir;

//==========================
// ENUM / IMPL
//==========================

pub struct MemBlockDevice {
    dev: Vec<[u8; super::BLOCK_SIZE]>,
}

impl MemBlockDevice {
    /// Creates a new memory block kdevice
    ///
    /// # Arguments
    ///
    /// * `len` - Length og the block
    pub fn new(len: usize) -> Self {
        let dev = vec![[0; super::BLOCK_SIZE]; len];

        Self { dev }
    }
}

#[derive(Clone)]
pub struct AtaBlockDevice {
    dev: sys::ata::Drive,
}

impl AtaBlockDevice {
    /// Creates a new ata block device
    ///
    /// # Arguments
    ///
    /// * `len` - Length of the block
    /// * `dsk` - ATA disk
    pub fn new(bus: u8, dsk: u8) -> Option<Self> {
        sys::ata::Drive::open(bus, dsk).map(|dev| Self { dev })
    }
}

pub enum BlockDevice {
    Mem(MemBlockDevice),
    Ata(AtaBlockDevice),
}

pub trait BlockDeviceIO {
    /// Reads a block device
    ///
    /// # Arguments
    ///
    /// * `addr` - Block device address
    /// * `buf` - Buffer to write into
    fn read(&self, addr: u32, buf: &mut [u8]) -> Result<(), ()>;

    /// Writes a block device
    ///
    /// # Arguments
    ///
    /// * `addr` - Block device address
    /// * `buf` - Buffer to write from
    fn write(&mut self, addr: u32, buf: &[u8]) -> Result<(), ()>;

    /// Gets the bock size
    fn block_size(&self) -> usize;

    /// Gets the blocks count
    fn block_count(&self) -> usize;
}

impl BlockDeviceIO for BlockDevice {
    fn read(&self, addr: u32, buf: &mut [u8]) -> Result<(), ()> {
        match self {
            BlockDevice::Mem(dev) => dev.read(addr, buf),
            BlockDevice::Ata(dev) => dev.read(addr, buf),
        }
    }

    fn write(&mut self, addr: u32, buf: &[u8]) -> Result<(), ()> {
        match self {
            BlockDevice::Mem(dev) => dev.write(addr, buf),
            BlockDevice::Ata(dev) => dev.write(addr, buf),
        }
    }

    fn block_size(&self) -> usize {
        match self {
            BlockDevice::Mem(dev) => dev.block_size() as usize,
            BlockDevice::Ata(dev) => dev.block_size() as usize,
        }
    }

    fn block_count(&self) -> usize {
        match self {
            BlockDevice::Mem(dev) => dev.block_count() as usize,
            BlockDevice::Ata(dev) => dev.block_count() as usize,
        }
    }
}

impl BlockDeviceIO for MemBlockDevice {
    fn read(&self, block_index: u32, buf: &mut [u8]) -> Result<(), ()> {
        // TODO: check for overflow
        buf[..].clone_from_slice(&self.dev[block_index as usize][..]);

        Ok(())
    }

    fn write(&mut self, block_index: u32, buf: &[u8]) -> Result<(), ()> {
        // TODO: check for overflow
        self.dev[block_index as usize][..].clone_from_slice(buf);

        Ok(())
    }

    fn block_size(&self) -> usize { super::BLOCK_SIZE }

    fn block_count(&self) -> usize { self.dev.len() }
}

impl BlockDeviceIO for AtaBlockDevice {
    fn read(&self, block_addr: u32, buf: &mut [u8]) -> Result<(), ()> {
        sys::ata::read(self.dev.bus, self.dev.dsk, block_addr, buf)
    }

    fn write(&mut self, block_addr: u32, buf: &[u8]) -> Result<(), ()> {
        sys::ata::write(self.dev.bus, self.dev.dsk, block_addr, buf)
    }

    fn block_size(&self) -> usize { self.dev.block_size() as usize }

    fn block_count(&self) -> usize { self.dev.block_count() as usize }
}

//==========================
// PUBLIC
//==========================

pub static BLOCK_DEVICE: Mutex<Option<BlockDevice>> = Mutex::new(None);

/// Mounts the memory
pub fn mount_mem() {
    let mem = sys::allocator::memory_size() / 2; // Half the allocatable memory
    let len = mem / super::BLOCK_SIZE; // TODO: take a size argument
    let dev = MemBlockDevice::new(len);

    *BLOCK_DEVICE.lock() = Some(BlockDevice::Mem(dev));
}

/// Formats the memory
pub fn format_mem() {
    debug_assert!(is_mounted());

    if let Some(sb) = SuperBlock::new() {
        sb.write();

        let root = Dir::root();

        BitmapBlock::alloc(root.addr());
    }
}

/// Mounts an ATA disk
///
/// # Arguments
///
/// * `bus` - ATA bus
/// * `dsk` - ATA disk
pub fn mount_ata(bus: u8, dsk: u8) {
    *BLOCK_DEVICE.lock() = AtaBlockDevice::new(bus, dsk).map(BlockDevice::Ata);
}

/// Formats the ATA
pub fn format_ata() {
    if let Some(sb) = SuperBlock::new() {
        // Write super_block
        sb.write();

        // Write zeros into block bitmaps
        super::bitmap_block::free_all();

        // Allocate root dir
        debug_assert!(is_mounted());
        let root = Dir::root();
        BitmapBlock::alloc(root.addr());
    }
}

/// Is the block mounted
pub fn is_mounted() -> bool { BLOCK_DEVICE.lock().is_some() }

/// Dismounts the block
pub fn dismount() { *BLOCK_DEVICE.lock() = None; }

//==========================
// TESTS
//==========================

#[cfg(test)]
mod tests {
    use super::*;

    #[test_case]
    fn test_mount_mem() {
        assert!(!is_mounted());
        mount_mem();
        assert!(is_mounted());
        dismount();
    }
}
