use alloc::vec;
use alloc::vec::Vec;

use super::block::LinkedBlock;
use super::dir::Dir;
use super::file::File;
use super::{dirname, file::FileIO, filename, realpath};
use crate::sys::clock::{Realtime, Uptime};
use crate::sys::console::Console;
use crate::sys::random::Random;

//==========================
// STRUCT / IMPL
//==========================

#[derive(PartialEq, Clone, Copy)]
#[repr(u8)]
pub enum DeviceType {
    Null     = 0,
    File     = 1,
    Console  = 2,
    Random   = 3,
    Uptime   = 4,
    Realtime = 5,
}

impl DeviceType {
    /// Creates a new buffer when creating a device
    pub fn buf(self) -> Vec<u8> {
        match self {
            DeviceType::Uptime | DeviceType::Realtime => {
                vec![self as u8, 0, 0, 0, 0, 0, 0, 0] // 8 bytes (f64)
            }
            DeviceType::Console => {
                vec![self as u8, 0, 0, 0] // 4 bytes (char)
            }
            _ => {
                vec![self as u8] // 1 byte
            }
        }
    }
}

#[derive(Debug, Clone)]
pub enum Device {
    Null,
    File(File),
    Console(Console),
    Random(Random),
    Uptime(Uptime),
    Realtime(Realtime),
}

impl From<u8> for Device {
    /// Creates a new device from an index
    ///
    /// # Arguments
    ///
    /// * `i` - Device index
    fn from(i: u8) -> Self {
        match i {
            i if i == DeviceType::Null as u8 => Device::Null,
            i if i == DeviceType::File as u8 => Device::File(File::new()),
            i if i == DeviceType::Console as u8 => Device::Console(Console::new()),
            i if i == DeviceType::Random as u8 => Device::Random(Random::new()),
            i if i == DeviceType::Uptime as u8 => Device::Uptime(Uptime::new()),
            i if i == DeviceType::Realtime as u8 => Device::Realtime(Realtime::new()),
            _ => unimplemented!(),
        }
    }
}

impl Device {
    /// Creates a new device
    ///
    /// # Arguments
    ///
    /// * `path` - Device path
    pub fn create(path: &str) -> Option<Self> {
        let pathname = realpath(path);
        let dirname = dirname(&pathname);
        let filename = filename(&pathname);

        if let Some(mut dir) = Dir::open(dirname) {
            if let Some(dir_entry) = dir.create_device(filename) {
                return Some(Device::File(dir_entry.into()));
            }
        }

        None
    }

    /// Opens a device
    ///
    /// # Arguments
    ///
    /// * `path` - Device path
    pub fn open(path: &str) -> Option<Self> {
        let pathname = realpath(path);
        let dirname = dirname(&pathname);
        let filename = filename(&pathname);

        if let Some(dir) = Dir::open(dirname) {
            if let Some(dir_entry) = dir.find(filename) {
                if dir_entry.is_device() {
                    let block = LinkedBlock::read(dir_entry.addr());
                    let data = block.data();
                    return Some(data[0].into());
                }
            }
        }

        None
    }
}

impl FileIO for Device {
    /// Reads a device
    ///
    /// # Arguments
    ///
    /// * `buf` - Buffer to write into
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, ()> {
        match self {
            Device::Null => Err(()),
            Device::File(io) => io.read(buf),
            Device::Console(io) => io.read(buf),
            Device::Random(io) => io.read(buf),
            Device::Uptime(io) => io.read(buf),
            Device::Realtime(io) => io.read(buf),
        }
    }

    /// Write a device
    ///
    /// # Arguments
    ///
    /// * `buf` - Buffer to read from
    fn write(&mut self, buf: &[u8]) -> Result<usize, ()> {
        match self {
            Device::Null => Ok(0),
            Device::File(io) => io.write(buf),
            Device::Console(io) => io.write(buf),
            Device::Random(io) => io.write(buf),
            Device::Uptime(io) => io.write(buf),
            Device::Realtime(io) => io.write(buf),
        }
    }
}
