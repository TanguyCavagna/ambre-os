use alloc::string::String;
use alloc::vec::Vec;
use core::convert::{From, TryInto};

use super::{dir::Dir, dirname, filename, realpath, FileType};

//==========================
// STRCT / IMPL
//==========================

#[derive(Clone)]
pub struct DirEntry {
    dir: Dir,
    addr: u32,

    // FileInfo
    kind: FileType,
    size: u32,
    time: u64,
    name: String,
}

impl DirEntry {
    /// Opens a directory
    ///
    /// # Arguments
    ///
    /// * `path` - Directory path
    pub fn open(path: &str) -> Option<Self> {
        let pathname = realpath(path);
        let dirname = dirname(&pathname);
        let filename = filename(&pathname);

        if let Some(dir) = Dir::open(dirname) {
            return dir.find(filename);
        }

        None
    }

    /// Creates a new directory entry
    ///
    /// # Arguments
    ///
    /// * `dir` - Parent directory
    /// * `kind` - Entry kind
    /// * `addr` - New address
    /// * `size` - New size
    /// * `time` - Creation time
    /// * `name` - Entry name
    pub fn new(dir: Dir, kind: FileType, addr: u32, size: u32, time: u64, name: &str) -> Self {
        let name = String::from(name);
        Self {
            dir,
            kind,
            addr,
            size,
            time,
            name,
        }
    }

    /// Gets the length of an empty entry
    pub fn empty_len() -> usize { 1 + 4 + 4 + 8 + 1 }

    /// Gets the length of an entry
    pub fn len(&self) -> usize { Self::empty_len() + self.name.len() }

    /// Is the entry empty
    pub fn is_empty(&self) -> bool { Self::empty_len() == self.len() }

    /// Gets the kind of entry
    pub fn kind(&self) -> FileType { self.kind }

    /// Is the entry a directory
    pub fn is_dir(&self) -> bool { self.kind == FileType::Dir }

    /// Is the entry a file
    pub fn is_file(&self) -> bool { self.kind == FileType::File }

    /// Is the entry a device
    pub fn is_device(&self) -> bool { self.kind == FileType::Device }

    /// Gets the entry address
    pub fn addr(&self) -> u32 { self.addr }

    /// Clones the parent directory
    pub fn dir(&self) -> Dir { self.dir.clone() }

    /// Gets the name
    pub fn name(&self) -> String { self.name.clone() }

    /// Gets the size
    pub fn size(&self) -> u32 { self.size }

    /// Gets the creation time
    pub fn time(&self) -> u64 { self.time }

    /// Gets the infos
    pub fn info(&self) -> FileInfo {
        FileInfo {
            kind: self.kind,
            name: self.name(),
            size: self.size(),
            time: self.time,
        }
    }
}

#[derive(Debug)]
pub struct FileInfo {
    kind: FileType,
    size: u32,
    time: u64,
    name: String,
}

impl FileInfo {
    /// Creates a new file info
    pub fn new() -> Self {
        Self {
            kind: FileType::File,
            name: String::new(),
            size: 0,
            time: 0,
        }
    }

    /// Creates the root file info
    pub fn root() -> Self {
        let kind = FileType::Dir;
        let name = String::new();
        let size = Dir::root().size() as u32;
        let time = 0;

        Self {
            kind,
            name,
            size,
            time,
        }
    }

    /// Gets the size
    pub fn size(&self) -> u32 { self.size }

    /// Gets the creation time
    pub fn time(&self) -> u64 { self.time }

    /// Gets the name
    pub fn name(&self) -> String { self.name.clone() }

    /// Gets the name
    pub fn kind(&self) -> FileType { self.kind }

    /// Is the entry a directory
    // TODO: Duplicated from dir entry
    pub fn is_dir(&self) -> bool { self.kind == FileType::Dir }

    /// Is the entry a file
    pub fn is_file(&self) -> bool { self.kind == FileType::File }

    /// Is the entry a device
    pub fn is_device(&self) -> bool { self.kind == FileType::Device }

    /// Converts the entry as bytes
    // TODO: Use bincode?
    pub fn as_bytes(&self) -> Vec<u8> {
        debug_assert!(self.name.len() < 256);

        let mut res = Vec::new();

        res.push(self.kind as u8);
        res.extend_from_slice(&self.size.to_be_bytes());
        res.extend_from_slice(&self.time.to_be_bytes());
        res.push(self.name.len() as u8);
        res.extend_from_slice(self.name.as_bytes());

        res
    }
}

impl From<&[u8]> for FileInfo {
    /// Creates a file info from an array
    ///
    /// buf[0] : kind
    /// buf[1..5] : size
    /// buf[5..13] : time
    /// buf[13] : end name
    /// buf[14..n] : name
    fn from(buf: &[u8]) -> Self {
        let kind = match buf[0] {
            // TODO: Add FileType::from(u8)
            0 => FileType::Dir,
            1 => FileType::File,
            2 => FileType::Device,
            _ => panic!(),
        };
        let size = u32::from_be_bytes(buf[1..5].try_into().unwrap());
        let time = u64::from_be_bytes(buf[5..13].try_into().unwrap());
        let i = 14 + buf[13] as usize;
        let name = String::from_utf8_lossy(&buf[14..i]).into();

        Self {
            kind,
            name,
            size,
            time,
        }
    }
}
