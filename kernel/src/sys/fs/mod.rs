use alloc::{
    format,
    string::{String, ToString},
};

pub use bitmap_block::BITMAP_SIZE;
pub use block_device::{dismount, format_ata, format_mem, is_mounted, mount_ata, mount_mem};
pub use device::{Device, DeviceType};
pub use dir::Dir;
use dir_entry::DirEntry;
pub use dir_entry::FileInfo;
pub use file::{File, FileIO, SeekFrom};
use super_block::SuperBlock;

use crate::sys;
pub use crate::sys::ata::BLOCK_SIZE;

pub mod bitmap_block;
pub mod block;
pub mod block_device;
pub mod device;
pub mod dir;
pub mod dir_entry;
pub mod file;
pub mod read_dir;
pub mod super_block;

pub const VERSION: u8 = 1;

//==========================
// ENUM / IMPL
//==========================

#[derive(Clone, Copy)]
#[repr(u8)]
pub enum OpenFlag {
    Read   = 1,
    Write  = 2,
    Create = 4,
    Dir    = 8,
    Device = 16,
}

impl OpenFlag {
    /// Checks if a flag is set
    ///
    /// # Arguments
    ///
    /// * `flags` - Flags to check
    fn is_set(&self, flags: usize) -> bool { flags & (*self as usize) != 0 }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum FileType {
    Dir    = 0,
    File   = 1,
    Device = 2,
}

#[derive(Debug, Clone)]
pub enum Resource {
    Dir(Dir),
    File(File),
    Device(Device),
}

impl FileIO for Resource {
    /// Reads an entry
    ///
    /// # Arguments
    ///
    /// * `buf` - Read buffer
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, ()> {
        match self {
            Resource::Dir(io) => io.read(buf),
            Resource::File(io) => io.read(buf),
            Resource::Device(io) => io.read(buf),
        }
    }

    /// Writes the buffer's content
    ///
    /// # Arguments
    ///
    /// * `buf` - Buffer to write
    fn write(&mut self, buf: &[u8]) -> Result<usize, ()> {
        match self {
            Resource::Dir(io) => io.write(buf),
            Resource::File(io) => io.write(buf),
            Resource::Device(io) => io.write(buf),
        }
    }
}

//==========================
// PUBLIC
//==========================

/// Gets the entry info
///
/// # Arguments
///
/// * `path` - Entry's path
pub fn info(path: &str) -> Option<FileInfo> {
    if path == "/" {
        return Some(FileInfo::root());
    }

    DirEntry::open(path).map(|e| e.info())
}

/// Gets the dirname of a path
///
/// # Arguments
///
/// * `path` - Full path
pub fn dirname(path: &str) -> &str {
    let n = path.len();
    let i = match path.rfind('/') {
        Some(0) => 1,
        Some(i) => i,
        None => n,
    };

    &path[0..i]
}

/// Gets the filename of a path
///
/// # Arguments
///
/// * `path` - Full path
pub fn filename(path: &str) -> &str {
    let n = path.len();
    let i = match path.rfind('/') {
        Some(i) => i + 1,
        None => 0,
    };

    &path[i..n]
}

/// Transforms a relative path like "foo.txt" into a real path like "/foo.txt"
///
/// # Arguments
///
/// * `path` - Path
pub fn realpath(path: &str) -> String {
    if path.starts_with('/') {
        path.into()
    } else {
        let dirname = sys::process::dir();
        let sep = if dirname.ends_with('/') { "" } else { "/" };

        format!("{}{}{}", dirname, sep, path)
    }
}

/// Opens an entry
///
/// # Arguments
///
/// * `path` - Entry's path
/// * `flags` - Flags to apply on the path entry
pub fn open(path: &str, flags: usize) -> Option<Resource> {
    if OpenFlag::Dir.is_set(flags) {
        let res = Dir::open(path);

        if res.is_none() && OpenFlag::Create.is_set(flags) {
            Dir::create(path)
        } else {
            res
        }
        .map(Resource::Dir)
    } else if OpenFlag::Device.is_set(flags) {
        let res = Device::open(path);

        if res.is_none() && OpenFlag::Create.is_set(flags) {
            Device::create(path)
        } else {
            res
        }
        .map(Resource::Device)
    } else {
        let res = File::open(path);

        if res.is_none() && OpenFlag::Create.is_set(flags) {
            File::create(path)
        } else {
            res
        }
        .map(Resource::File)
    }
}

/// Deletes an entry
///
/// # Arguments
///
/// * `path` - Entry's path
pub fn delete(path: &str) -> Result<(), ()> {
    if let Some(info) = info(path) {
        if info.is_file() {
            return File::delete(path);
        } else if info.is_dir() {
            return Dir::delete(path);
        }
    }

    Err(())
}

/// Format the path
///
/// # Arguments
///
/// * `path` - Path to format
pub fn canonicalize(path: &str) -> Result<String, ()> {
    match sys::process::env("HOME") {
        Some(home) => {
            if path.starts_with('~') {
                Ok(path.replace('~', &home))
            } else {
                Ok(path.to_string())
            }
        }

        None => Ok(path.to_string()),
    }
}

/// Gets the disk size
pub fn disk_size() -> usize { (SuperBlock::read().block_count as usize) * BLOCK_SIZE }

/// Gets the disk used space
pub fn disk_used() -> usize { (SuperBlock::read().alloc_count as usize) * BLOCK_SIZE }

/// Gets the disk free space
pub fn disk_free() -> usize { disk_size() - disk_used() }

pub fn init() {
    for bus in 0..2 {
        for dsk in 0..2 {
            if SuperBlock::check_ata(bus, dsk) {
                log!("MFS Superblock found in ATA {}:{}\n", bus, dsk);
                mount_ata(bus, dsk);
                return;
            }
        }
    }
}
