//! Global Descriptor Table module
//!
//! It is used to register memory segments to tell the CPU where, e.g. the TSS (Task State Segment) is located.
//! It is a relict that was used for memory segmentation before the paging system became the de
//! facto standard, but it is necessary for in 64-bit mode for various things such as kernel/user
//! mode configuration or TSS loading.

use lazy_static::lazy_static;
use x86_64::registers::segmentation::DS;
use x86_64::structures::gdt::{Descriptor, GlobalDescriptorTable, SegmentSelector};
use x86_64::structures::tss::TaskStateSegment;
use x86_64::VirtAddr;

use crate::println;

//==========================
// STRUCT
//==========================

pub const DOUBLE_FAULT_IST_INDEX: u16 = 0;
pub const PAGE_FAULT_IST_INDEX: u16 = 1;
pub const GENERAL_PROTECTION_FAULT_IST_INDEX: u16 = 2;

pub struct Selectors {
    code_selector: SegmentSelector,
    tss_selector: SegmentSelector,
    data: SegmentSelector,
    pub user_code: SegmentSelector,
    pub user_data: SegmentSelector,
}

//==========================
// REGISTER TABLES
//==========================

lazy_static! {
    /// Creating the TaskStateSegment and the stack
    ///
    /// TSS is a structure that holds, in 64-bit mode, two stack tables. The IST (Interrupt Stack
    /// Table) and the PST (Privilege Stack Table). In addition, the only other things stored is
    /// the I/O Map Base Address (only common things between 32-bit and 64-bit mode).
    static ref TSS: TaskStateSegment = {
        let mut tss = TaskStateSegment::new();

        tss.interrupt_stack_table[DOUBLE_FAULT_IST_INDEX as usize] = {
            const STACK_SIZE: usize = 4096 * 5;
            static mut STACK: [u8; STACK_SIZE] = [0; STACK_SIZE];

            let stack_start = VirtAddr::from_ptr(unsafe { &STACK });

            // stack end
            stack_start + STACK_SIZE
        };

        tss
    };
}

lazy_static! {
    ///Creating the GDT
    pub static ref GDT: (GlobalDescriptorTable, Selectors) = {
        let mut gdt = GlobalDescriptorTable::new();

        let code_selector = gdt.add_entry(Descriptor::kernel_code_segment());
        let tss_selector = gdt.add_entry(Descriptor::tss_segment(&TSS));
        let data = gdt.add_entry(Descriptor::kernel_data_segment());
        let user_code = gdt.add_entry(Descriptor::user_code_segment());
        let user_data = gdt.add_entry(Descriptor::user_data_segment());

        (gdt, Selectors { code_selector, tss_selector, data, user_code, user_data })
    };
}

//==========================
// INITIALIZATION
//==========================

/// Initialize the GDT
pub fn init() {
    use x86_64::instructions::segmentation::{Segment, CS};
    use x86_64::instructions::tables;

    if !cfg!(feature = "quiet") {
        log!("Initializing GDT... ");
    }

    GDT.0.load();

    unsafe {
        CS::set_reg(GDT.1.code_selector); // reload the Code Segment register
        DS::set_reg(GDT.1.data);
        tables::load_tss(GDT.1.tss_selector); // load the TSS in the new GDT registers
    }

    if !cfg!(feature = "quiet") {
        let csi_color = crate::sys::console::Style::color("LightGreen");
        let csi_reset = crate::sys::console::Style::reset();
        println!("{}[ok]{}", csi_color, csi_reset);
    }
}
