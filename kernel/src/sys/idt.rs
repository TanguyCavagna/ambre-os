//! Interrupts handler for internal and external interrupt. It allows to talk with the Primary Interrupt Controller and
//! the Secondary Interrupt Controller. Those are the following :
//!
//!                          ____________                          ____________
//!     Real Time Clock --> |            |   Timer -------------> |            |
//!     ACPI -------------> |            |   Keyboard-----------> |            |      _____
//!     Available --------> | Secondary  |----------------------> | Primary    |     |     |
//!     Available --------> | Interrupt  |   Serial Port 2 -----> | Interrupt  |---> | CPU |
//!     Mouse ------------> | Controller |   Serial Port 1 -----> | Controller |     |_____|
//!     Co-Processor -----> |            |   Parallel Port 2/3 -> |            |
//!     Primary ATA ------> |            |   Floppy disk -------> |            |
//!     Secondary ATA ----> |____________|   Parallel Port 1----> |____________|

use lazy_static::lazy_static;
use spin::Mutex;
use x86_64::instructions::interrupts;
use x86_64::instructions::port::Port;
use x86_64::structures::idt::{InterruptDescriptorTable, InterruptStackFrame, PageFaultErrorCode};

use crate::println;
use crate::sys;

// Interrupt Controllers addresses
const PIC1: u16 = 0x21;
const PIC2: u16 = 0xA1;

//==========================
// ENUMS / IMPL
//==========================

#[derive(Debug, Clone, Copy)]
#[repr(u8)]
pub enum InterruptIndex {
    Timer = sys::pic::PIC_1_OFFSET,
    Keyboard,
    SIC, // Secondary Interrupt Controller
    SerialPort2,
    SerialPort1,
    ParallelPort2_3,
    FloppyDisk,
    ParallelPort1,
    RTC,
    ACPI,
    Available1,
    Available2,
    Mouse,
    CoProcessor,
    PrimaryATA,
    SecondaryATA,
}

impl InterruptIndex {
    fn as_u8(self) -> u8 { self as u8 }

    fn as_usize(self) -> usize { usize::from(self.as_u8()) }

    pub fn as_index(self) -> u8 { self.as_u8() - sys::pic::PIC_1_OFFSET }
}

fn interrupt_index(irq: u8) -> u8 { sys::pic::PIC_1_OFFSET + irq }

//==========================
// INTERRUPT HANDLER INIT MACRO
//==========================

/// Create interrupt handler with the given name and interrupt index
macro_rules! irq_handler {
    ($handler:ident, $irq:expr) => {
        pub extern "x86-interrupt" fn $handler(_stack_frame: InterruptStackFrame) {
            let handlers = IRQ_HANDLERS.lock();
            handlers[$irq]();
            unsafe {
                sys::pic::PICS.lock().notify_end_of_interrupt(interrupt_index($irq));
            }
        }
    };
}

//==========================
// EXCEPTION HANDLERS
//==========================

/// NOP handler
fn default_irq_handler() {}

/// CPU breakpoint handler
extern "x86-interrupt" fn breakpoint_handler(stack_frame: InterruptStackFrame) {
    println!("EXCEPTION: BREAKPOINT\n{:#?}", stack_frame);
}

/// CPU stack segment fault
extern "x86-interrupt" fn stack_segment_fault_handler(
    stack_frame: InterruptStackFrame,
    _error_code: u64,
) {
    println!("EXCEPTION: STACK SEGMENT FAULT\n{:#?}", stack_frame);
}

/// CPU segment not present
extern "x86-interrupt" fn segment_not_present_handler(
    stack_frame: InterruptStackFrame,
    _error_code: u64,
) {
    println!("EXCEPTION: SEGMENT NOT PRESENT\n{:#?}", stack_frame);
}

/// CPU double fault handler
extern "x86-interrupt" fn double_fault_handler(
    stack_frame: InterruptStackFrame,
    _error_code: u64,
) -> ! {
    panic!("EXCEPTION: DOUBLE FAULT\n{:#?}", stack_frame);
}

/// CPU page fault handler
extern "x86-interrupt" fn page_fault_handler(
    stack_frame: InterruptStackFrame,
    error_code: PageFaultErrorCode,
) {
    use x86_64::registers::control::Cr2;

    println!("EXCEPTION: PAGE FAULT");
    println!("Accessed Address: {:?}", Cr2::read());
    println!("Error Code: {:?}", error_code);
    println!("{:#?}", stack_frame);
}

/// CPU general protection fault
extern "x86-interrupt" fn general_protection_fault_handler(
    stack_frame: InterruptStackFrame,
    _error_code: u64,
) {
    panic!("EXCEPTION: GENERAL PROTECTION FAULT\n{:#?}", stack_frame);
}

//==========================
// SETTERS
//==========================

pub fn set_irq_handler(irq: u8, handler: fn()) {
    interrupts::without_interrupts(|| {
        let mut handlers = IRQ_HANDLERS.lock();
        handlers[irq as usize] = handler;

        clear_irq_mask(irq);
    });
}

pub fn set_irq_mask(irq: u8) {
    let mut port: Port<u8> = Port::new(if irq < 8 { PIC1 } else { PIC2 });
    unsafe {
        let value = port.read() | (1 << (if irq < 8 { irq } else { irq - 8 }));
        port.write(value);
    }
}

pub fn clear_irq_mask(irq: u8) {
    let mut port: Port<u8> = Port::new(if irq < 8 { PIC1 } else { PIC2 });
    unsafe {
        let value = port.read() & !(1 << if irq < 8 { irq } else { irq - 8 });
        port.write(value);
    }
}

//==========================
// REGISTER RELOAD
//==========================

lazy_static! {
    /// Custom interrupts handlers for the 16 sources possible
    ///
    /// ## Informations
    ///
    /// By default, all handlers do nothing
    pub static ref IRQ_HANDLERS: Mutex<[fn(); 16]> = Mutex::new([default_irq_handler; 16]);

    /// Create the Interrupt Descriptor Table
    ///
    /// In order to catch and handle exceptions, the Interrupt Descriptor Table is mendarody. In
    /// this table we can specify a handler function for each CPU exception. Given that the
    /// hardware is using this table directly, we need to have a predefined format.
    ///
    /// #### see: <https://os.phil-opp.com/cpu-exceptions/#the-interrupt-descriptor-table>
    static ref IDT: InterruptDescriptorTable = {
        let mut idt = InterruptDescriptorTable::new();

        idt.breakpoint.set_handler_fn(breakpoint_handler);
        idt.stack_segment_fault.set_handler_fn(stack_segment_fault_handler);
        idt.segment_not_present.set_handler_fn(segment_not_present_handler);

        unsafe {
            idt.double_fault.set_handler_fn(double_fault_handler).set_stack_index(sys::gdt::DOUBLE_FAULT_IST_INDEX);
            idt.page_fault.set_handler_fn(page_fault_handler).set_stack_index(sys::gdt::PAGE_FAULT_IST_INDEX);
            idt.general_protection_fault.set_handler_fn(general_protection_fault_handler).set_stack_index(sys::gdt::GENERAL_PROTECTION_FAULT_IST_INDEX);
            // TODO idt[0x80] is syscall. Handling later
        }

        // All other interrupts handling
        idt[InterruptIndex::Timer.as_usize()].set_handler_fn(irq0_handler);
        idt[InterruptIndex::Keyboard.as_usize()].set_handler_fn(irq1_handler);
        idt[InterruptIndex::SIC.as_usize()].set_handler_fn(irq2_handler);
        idt[InterruptIndex::SerialPort2.as_usize()].set_handler_fn(irq3_handler);
        idt[InterruptIndex::SerialPort1.as_usize()].set_handler_fn(irq4_handler);
        idt[InterruptIndex::ParallelPort2_3.as_usize()].set_handler_fn(irq5_handler);
        idt[InterruptIndex::FloppyDisk.as_usize()].set_handler_fn(irq6_handler);
        idt[InterruptIndex::ParallelPort1.as_usize()].set_handler_fn(irq7_handler);
        idt[InterruptIndex::RTC.as_usize()].set_handler_fn(irq8_handler);
        idt[InterruptIndex::ACPI.as_usize()].set_handler_fn(irq9_handler);
        idt[InterruptIndex::Available1.as_usize()].set_handler_fn(irq10_handler);
        idt[InterruptIndex::Available2.as_usize()].set_handler_fn(irq11_handler);
        idt[InterruptIndex::Mouse.as_usize()].set_handler_fn(irq12_handler);
        idt[InterruptIndex::CoProcessor.as_usize()].set_handler_fn(irq13_handler);
        idt[InterruptIndex::PrimaryATA.as_usize()].set_handler_fn(irq14_handler);
        idt[InterruptIndex::SecondaryATA.as_usize()].set_handler_fn(irq15_handler);

        idt
    };
}

//==========================
// INITIALIZATION
//==========================

irq_handler!(irq0_handler, 0);
irq_handler!(irq1_handler, 1);
irq_handler!(irq2_handler, 2);
irq_handler!(irq3_handler, 3);
irq_handler!(irq4_handler, 4);
irq_handler!(irq5_handler, 5);
irq_handler!(irq6_handler, 6);
irq_handler!(irq7_handler, 7);
irq_handler!(irq8_handler, 8);
irq_handler!(irq9_handler, 9);
irq_handler!(irq10_handler, 10);
irq_handler!(irq11_handler, 11);
irq_handler!(irq12_handler, 12);
irq_handler!(irq13_handler, 13);
irq_handler!(irq14_handler, 14);
irq_handler!(irq15_handler, 15);

/// Initialize the IDT
pub fn init() {
    if !cfg!(feature = "quiet") {
        log!("Initializing IDT... ");
    }

    IDT.load();

    if !cfg!(feature = "quiet") {
        let csi_color = crate::sys::console::Style::color("LightGreen");
        let csi_reset = crate::sys::console::Style::reset();
        println!("{}[ok]{}", csi_color, csi_reset);
    }
}

//==========================
// TESTS
//==========================

#[cfg(test)]
mod tests {
    #[test_case]
    fn test_breakpoint_exception() { x86_64::instructions::interrupts::int3(); }
}
