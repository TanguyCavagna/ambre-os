use alloc::vec;
use alloc::vec::Vec;
use alloc::{collections::BTreeMap, sync::Arc};
use core::sync::atomic::{AtomicBool, AtomicU64, Ordering};

use smoltcp::phy::Device;
use smoltcp::{
    iface::{InterfaceBuilder, NeighborCache, Routes},
    phy::{DeviceCapabilities, Medium},
    time::Instant,
    wire::{EthernetAddress, IpCidr, Ipv4Address},
};
use spin::Mutex;

use crate::sys;

mod rtl8139;

//==========================
// STRUCT / ENUMS / TRAIT / IMPL
//==========================

pub struct Config {
    debug: AtomicBool,
    mac: Mutex<Option<EthernetAddress>>,
}

impl Config {
    /// Creates a new config
    fn new() -> Self {
        Self {
            debug: AtomicBool::new(false),
            mac: Mutex::new(None),
        }
    }

    /// Is debugging enable
    fn is_debug_enabled(&self) -> bool { self.debug.load(Ordering::Relaxed) }

    /// Gets the MAC address
    #[allow(dead_code)]
    fn mac(&self) -> Option<EthernetAddress> { *self.mac.lock() }

    /// Updates the MAC address
    ///
    /// # Arguments
    ///
    /// * `mac` - New MAC address
    fn update_mac(&self, mac: EthernetAddress) { *self.mac.lock() = Some(mac); }

    /// Enables debugging
    pub fn enable_debug(&self) { self.debug.store(true, Ordering::Relaxed); }

    /// Disables debugging
    pub fn disable_debug(&self) { self.debug.store(false, Ordering::Relaxed) }
}

pub struct Stats {
    rx_bytes_count: AtomicU64,
    tx_bytes_count: AtomicU64,
    rx_packets_count: AtomicU64,
    tx_packets_count: AtomicU64,
}

impl Stats {
    /// Creates new Stats
    fn new() -> Self {
        Self {
            rx_bytes_count: AtomicU64::new(0),
            tx_bytes_count: AtomicU64::new(0),
            rx_packets_count: AtomicU64::new(0),
            tx_packets_count: AtomicU64::new(0),
        }
    }

    /// Counts the bytes received
    pub fn rx_bytes_count(&self) -> u64 { self.rx_bytes_count.load(Ordering::Relaxed) }

    /// Counts the bytes transmitted
    pub fn tx_bytes_count(&self) -> u64 { self.tx_bytes_count.load(Ordering::Relaxed) }

    /// Counts the packets received
    pub fn rx_packets_count(&self) -> u64 { self.rx_packets_count.load(Ordering::Relaxed) }

    /// Counts the packets transmitted
    pub fn tx_packets_count(&self) -> u64 { self.tx_packets_count.load(Ordering::Relaxed) }

    /// Adds bytes to Rx
    ///
    /// # Arguments
    ///
    /// * `bytes_count` - Number of bytes to add
    pub fn rx_add(&self, bytes_count: u64) {
        self.rx_packets_count.fetch_add(1, Ordering::SeqCst);
        self.rx_bytes_count.fetch_add(bytes_count, Ordering::SeqCst);
    }

    /// Adds bytes to Tx
    ///
    /// # Arguments
    ///
    /// * `bytes_count` - Number of bytes to add
    pub fn tx_add(&self, bytes_count: u64) {
        self.tx_packets_count.fetch_add(1, Ordering::SeqCst);
        self.tx_bytes_count.fetch_add(bytes_count, Ordering::SeqCst);
    }
}

#[doc(hidden)]
pub struct RxToken {
    buffer: Vec<u8>,
}

impl smoltcp::phy::RxToken for RxToken {
    fn consume<R, F>(mut self, _timestamp: Instant, f: F) -> smoltcp::Result<R>
    where
        F: FnOnce(&mut [u8]) -> smoltcp::Result<R>,
    {
        f(&mut self.buffer)
    }
}

#[doc(hidden)]
pub struct TxToken {
    device: EthernetDevice,
}

impl smoltcp::phy::TxToken for TxToken {
    fn consume<R, F>(mut self, _timestamp: Instant, len: usize, f: F) -> smoltcp::Result<R>
    where
        F: FnOnce(&mut [u8]) -> smoltcp::Result<R>,
    {
        let config = self.device.config();
        let mut buf = self.device.next_tx_buffer(len);
        let res = f(&mut buf);
        if res.is_ok() {
            if config.is_debug_enabled() {
                debug!("NET Packet Transmitted");
                // usr::hex::print_hex(&buf);
            }
            self.device.transmit_packet(len);
            self.device.stats().tx_add(len as u64);
        }
        res
    }
}

#[derive(Clone)]
pub enum EthernetDevice {
    RTL8139(rtl8139::Device),
    // PCNET,
    // E2000,
    // VirtIO,
}

pub trait EthernetDeviceIO {
    /// Setup configration
    fn config(&self) -> Arc<Config>;

    /// Adds stats handling
    fn stats(&self) -> Arc<Stats>;

    /// Allows device to receive packet
    ///
    /// # Arguments
    ///
    /// * `len` - Packet length
    fn receive_packet(&mut self) -> Option<Vec<u8>>;

    /// ALlows device to transmit packet
    ///
    /// # Arguments
    ///
    /// * `len` - Packet length
    fn transmit_packet(&mut self, len: usize);

    /// Gets the next Tx buffer of given length
    ///
    /// # Arguments
    ///
    /// * `len` - Buffer length
    fn next_tx_buffer(&mut self, len: usize) -> &mut [u8];
}

impl EthernetDeviceIO for EthernetDevice {
    fn config(&self) -> Arc<Config> {
        match self {
            EthernetDevice::RTL8139(dev) => dev.config(),
        }
    }

    fn stats(&self) -> Arc<Stats> {
        match self {
            EthernetDevice::RTL8139(dev) => dev.stats(),
        }
    }

    fn receive_packet(&mut self) -> Option<Vec<u8>> {
        match self {
            EthernetDevice::RTL8139(dev) => dev.receive_packet(),
        }
    }

    fn transmit_packet(&mut self, len: usize) {
        match self {
            EthernetDevice::RTL8139(dev) => dev.transmit_packet(len),
        }
    }

    fn next_tx_buffer(&mut self, len: usize) -> &mut [u8] {
        match self {
            EthernetDevice::RTL8139(dev) => dev.next_tx_buffer(len),
        }
    }
}

impl<'a> smoltcp::phy::Device<'a> for EthernetDevice {
    type RxToken = RxToken;
    type TxToken = TxToken;

    fn capabilities(&self) -> DeviceCapabilities {
        let mut caps = DeviceCapabilities::default();

        caps.max_transmission_unit = 1500;
        caps.max_burst_size = Some(1);

        caps
    }

    fn receive(&'a mut self) -> Option<(Self::RxToken, Self::TxToken)> {
        if let Some(buffer) = self.receive_packet() {
            if self.config().is_debug_enabled() {
                debug!("NET Packet Received");
                // usr::hex::print_hex(&buffer);
            }

            self.stats().rx_add(buffer.len() as u64);

            let rx = RxToken { buffer };
            let tx = TxToken {
                device: self.clone(),
            };

            Some((rx, tx))
        } else {
            None
        }
    }

    fn transmit(&'a mut self) -> Option<Self::TxToken> {
        let tx = TxToken {
            device: self.clone(),
        };

        Some(tx)
    }
}

//==========================
// PRIVATE
//==========================

/// Finds the base offset of the given device
///
/// # Arguments
///
/// * `vendor_id` - Vendor ID
/// * `device_id` - Device ID
fn find_pci_io_base(vendor_id: u16, device_id: u16) -> Option<u16> {
    if let Some(mut pci_device) = sys::pci::find_device(vendor_id, device_id) {
        pci_device.enable_bus_mastering();
        let io_base = (pci_device.base_addresses[0] as u16) & 0xFFF0;

        Some(io_base)
    } else {
        None
    }
}

//==========================
// PUBLIC
//==========================

pub type Interface = smoltcp::iface::Interface<'static, EthernetDevice>;
pub static IFACE: Mutex<Option<Interface>> = Mutex::new(None);

pub fn init() {
    let add_interface = |device: EthernetDevice, name| {
        if let Some(mac) = device.config().mac() {
            log!("NET {} MAC {}\n", name, mac);

            let neighbor_cache = NeighborCache::new(BTreeMap::new());
            let routes = Routes::new(BTreeMap::new());
            let ip_addrs = [IpCidr::new(Ipv4Address::UNSPECIFIED.into(), 0)];
            let medium = device.capabilities().medium;

            let mut builder =
                InterfaceBuilder::new(device, vec![]).ip_addrs(ip_addrs).routes(routes);

            if medium == Medium::Ethernet {
                builder = builder.hardware_addr(mac.into()).neighbor_cache(neighbor_cache);
            }

            let iface = builder.finalize();

            *IFACE.lock() = Some(iface);
        }
    };

    if let Some(io_base) = find_pci_io_base(rtl8139::RTL8139_VENDOR_ID, rtl8139::RTL8139_DEVICE_ID)
    {
        add_interface(
            EthernetDevice::RTL8139(rtl8139::Device::new(io_base)),
            "RTL8139",
        );
    }
}
