// for any informations about the registers, see <http://realtek.info/pdf/rtl8139cp.pdf>

use alloc::{sync::Arc, vec::Vec};
use core::sync::atomic::{AtomicUsize, Ordering};

use smoltcp::wire::EthernetAddress;
use x86_64::instructions::port::Port;

use super::{Config, EthernetDeviceIO, Stats};
use crate::sys::allocator::PhysBuf;

pub const RTL8139_VENDOR_ID: u16 = 0x10EC;
pub const RTL8139_DEVICE_ID: u16 = 0x8139;

// 00 = 8K + 16 bytes
// 01 = 16K + 16 bytes
// 10 = 32K + 16 bytes
// 11 = 64K + 16 bytes
const RX_BUFFER_IDX: usize = 0;
const RX_BUFFER_PAD: usize = 16;
const RX_BUFFER_LEN: usize = (8129 << RX_BUFFER_IDX) + RX_BUFFER_PAD;

const TX_BUFFER_LEN: usize = 4096;
const TX_BUFFERS_COUNT: usize = 4;

const ROK: u16 = 0x01; // Receive OK
const MTU: usize = 1500;

//==========================
// Command Register
//==========================

const CR_OFFSET: u16 = 0x0037;
// commands, see <http://realtek.info/pdf/rtl8139cp.pdf#%5B%7B%22num%22%3A184%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2C0%2C502%2Cnull%5D>
const CR_RST: u8 = 1 << 4; // reset
const CR_RE: u8 = 1 << 3; // receiver enable
const CR_TE: u8 = 1 << 2; // transmitter enable
const CR_BUFE: u8 = 1 << 0; // buffer empty

//==========================
// Receive Configuration Register
//==========================

const RCR_OFFSET: u16 = 0x0044;
const RCR_RBLEN: u32 = (RX_BUFFER_IDX << 11) as u32; // rx buffer lenght

// When the WRAP bit is set, the NIC (Network Interface Controller) will keep moving the rest
// of the packet data into the memory immediately after the
// end of the Rx buffer instead of going back to the begining
// of the buffer. So the buffer must have an additionnal 1500 bytes.
// This bit is invalid when Rx buffer is selected to 64K bytes.
const RCR_WRAP: u32 = 1 << 7;
const RCR_AB: u32 = 1 << 3; // accept broadcasr packets
const RCR_AM: u32 = 1 << 2; // accept multicast packets
const RCR_APM: u32 = 1 << 1; // accept physical match packets
const RCR_AAP: u32 = 1 << 0; // accept all packets

//==========================
// Transmit Configuration Register
//==========================

const TCR_OFFSET: u16 = 0x0040;
const TCR_IFG: u32 = 3 << 24; // interframe gap time

// Max DMA Burst Size per Tx DMA Burst
// 000 = 16 bytes
// 001 = 32 bytes
// 010 = 64 bytes
// 011 = 128 bytes
// 100 = 256 bytes
// 101 = 512 bytes
// 110 = 1024 bytes
// 111 = 2048 bytes
const TCR_MXDMA2: u32 = 1 << 10;
const TCR_MXDMA1: u32 = 1 << 9;
const TCR_MXDMA0: u32 = 1 << 8;

//==========================
// Interrupt Mask Register
//==========================

const IMR_OFFSET: u16 = 0x003C;
const IMR_TOK: u16 = 1 << 2; // Transmit OK Interrupt
const IMR_ROK: u16 = 1 << 0; // Receive OK Interrupt

//==========================
// Transmit Status Register
//==========================

const TSR_OFFSET: u16 = 0x0010;
const TSR_TOK: u32 = 1 << 15; // Transmit OK
const TSR_OWN: u32 = 1 << 13; // DMA operation completed

//==========================
// STRUCT / IMPL
//==========================

#[derive(Clone)]
pub struct Ports {
    pub mac: [Port<u8>; 6],                      // ID Registers (IDR0 ... IDR5)
    pub tx_cmds: [Port<u32>; TX_BUFFERS_COUNT],  // Transmit Status of Descriptors (TSD0 .. TSD3)
    pub tx_addrs: [Port<u32>; TX_BUFFERS_COUNT], /* Transmit Start Address of Descriptor0 (TSAD0 .. TSAD3) */
    pub config1: Port<u8>,                       // Configuration Register 1 (CONFIG1)
    pub rx_addr: Port<u32>,                      // Receive (Rx) Buffer Start Address (RBSTART)
    pub capr: Port<u16>,                         // Current Address of Packet Read (CAPR)
    pub cbr: Port<u16>,                          // Current Buffer Address (CBR)
    pub cmd: Port<u8>,                           // Command Register (CR)
    pub imr: Port<u16>,                          // Interrupt Mask Register (IMR)
    pub isr: Port<u16>,                          // Interrupt Status Register (ISR)
    pub tx_config: Port<u32>,                    // Transmit (Tx) Configuration Register (TCR)
    pub rx_config: Port<u32>,                    // Receive (Rx) Configuration Register (RCR)
}

impl Ports {
    /// Creates a new Ports instance for the network controller
    ///
    /// # Arguments
    ///
    /// * `io_base` - Base offset of the device
    pub fn new(io_base: u16) -> Self {
        Self {
            mac: [
                Port::new(io_base + 0x00),
                Port::new(io_base + 0x01),
                Port::new(io_base + 0x02),
                Port::new(io_base + 0x03),
                Port::new(io_base + 0x04),
                Port::new(io_base + 0x05),
            ],
            tx_cmds: [
                Port::new(io_base + TSR_OFFSET),
                Port::new(io_base + TSR_OFFSET + 0x04),
                Port::new(io_base + TSR_OFFSET + 0x08),
                Port::new(io_base + TSR_OFFSET + 0x0C),
            ],
            tx_addrs: [
                Port::new(io_base + 0x20),
                Port::new(io_base + 0x24),
                Port::new(io_base + 0x28),
                Port::new(io_base + 0x2C),
            ],
            config1: Port::new(io_base + 0x52),
            rx_addr: Port::new(io_base + 0x30),
            capr: Port::new(io_base + 0x38),
            cbr: Port::new(io_base + 0x3A),
            cmd: Port::new(io_base + CR_OFFSET),
            imr: Port::new(io_base + IMR_OFFSET),
            isr: Port::new(io_base + 0x3E),
            tx_config: Port::new(io_base + TCR_OFFSET),
            rx_config: Port::new(io_base + RCR_OFFSET),
        }
    }

    /// Creates the MAC address
    fn mac(&mut self) -> [u8; 6] {
        unsafe {
            [
                self.mac[0].read(),
                self.mac[1].read(),
                self.mac[2].read(),
                self.mac[3].read(),
                self.mac[4].read(),
                self.mac[5].read(),
            ]
        }
    }
}

#[derive(Clone)]
pub struct Device {
    config: Arc<Config>,
    stats: Arc<Stats>,
    ports: Ports,
    rx_buffer: PhysBuf,
    rx_offset: usize,
    tx_buffers: [PhysBuf; TX_BUFFERS_COUNT],
    tx_id: Arc<AtomicUsize>,
}

impl Device {
    /// Creates a new device
    ///
    /// # Arguments
    ///
    /// * `io_base` - Base device offset
    pub fn new(io_base: u16) -> Self {
        let mut device = Self {
            config: Arc::new(Config::new()),
            stats: Arc::new(Stats::new()),
            ports: Ports::new(io_base),

            // Add MTU to RX_BUFFER_LEN if RCR_WRAP is set
            rx_buffer: PhysBuf::new(RX_BUFFER_LEN + MTU),

            rx_offset: 0,
            tx_buffers: [(); TX_BUFFERS_COUNT].map(|_| PhysBuf::new(TX_BUFFER_LEN)),

            // Before a transmission begin the id is incremented,
            // so the first transimission will start at 0.
            tx_id: Arc::new(AtomicUsize::new(TX_BUFFERS_COUNT - 1)),
        };

        device.init();

        device
    }

    /// Initialize the device
    fn init(&mut self) {
        // Power on
        unsafe { self.ports.config1.write(0) }

        // Software reset
        unsafe {
            self.ports.cmd.write(CR_RST);
            while self.ports.cmd.read() & CR_RST != 0 {}
        }

        // Enable Receive and Transmitter
        unsafe { self.ports.cmd.write(CR_RE | CR_TE) }

        // Read MAC addr
        self.config.update_mac(EthernetAddress::from_bytes(&self.ports.mac()));

        // Get physical address of rx_buffer
        let rx_addr = self.rx_buffer.addr();

        // Init Receive buffer
        unsafe { self.ports.rx_addr.write(rx_addr as u32) }

        for i in 0..4 {
            // Get physical address of each tx_buffer
            let tx_addr = self.tx_buffers[i].addr();

            // Init Transmit buffer
            unsafe { self.ports.tx_addrs[i].write(tx_addr as u32) }
        }

        // Set interrupts
        unsafe { self.ports.imr.write(IMR_TOK | IMR_ROK) }

        // Configure receive buffer (RCR)
        unsafe {
            self.ports.rx_config.write(RCR_RBLEN | RCR_WRAP | RCR_AB | RCR_AM | RCR_APM | RCR_AAP)
        }

        // Configure transmit buffer (TCR)
        unsafe {
            self.ports.tx_config.write(TCR_IFG | TCR_MXDMA0 | TCR_MXDMA1 | TCR_MXDMA2);
        }
    }
}

impl EthernetDeviceIO for Device {
    /// Setups config
    fn config(&self) -> Arc<Config> { self.config.clone() }

    /// Setups stats
    fn stats(&self) -> Arc<Stats> { self.stats.clone() }

    /// Reads the received packet
    // RxToken buffer, when not empty, will contains:
    // [header            (2 bytes)]
    // [length            (2 bytes)]
    // [packet   (length - 4 bytes)]
    // [crc               (4 bytes)]
    fn receive_packet(&mut self) -> Option<Vec<u8>> {
        let cmd = unsafe { self.ports.cmd.read() };

        if (cmd & CR_BUFE) == CR_BUFE {
            return None;
        }

        let capr = unsafe { self.ports.capr.read() }; // Current Address of Packet Read
        let cbr = unsafe { self.ports.cbr.read() }; // Current Buffer Address

        // CAPR starts at 65520 and with the pad it overflows to 0
        let offset = ((capr as usize) + RX_BUFFER_PAD) % (1 << 16);

        let header =
            u16::from_le_bytes(self.rx_buffer[(offset + 0)..(offset + 2)].try_into().unwrap());

        // Packet has not been received properly
        if header & ROK != ROK {
            unsafe { self.ports.capr.write(cbr) };

            return None;
        }

        let n = u16::from_le_bytes(self.rx_buffer[(offset + 2)..(offset + 4)].try_into().unwrap())
            as usize;

        // Update buffer read pointer
        self.rx_offset = (offset + n + 4 + 3) & !3;

        unsafe {
            self.ports.capr.write((self.rx_offset - RX_BUFFER_PAD) as u16);
        }

        Some(self.rx_buffer[(offset + 4)..(offset + n)].to_vec())
    }

    /// Transmit packet
    ///
    /// # Arguments
    ///
    /// * `len` - Packet length
    fn transmit_packet(&mut self, len: usize) {
        let tx_id = self.tx_id.load(Ordering::SeqCst);
        let mut cmd_port = self.ports.tx_cmds[tx_id].clone();

        unsafe {
            // Fill in Transmit Status: the size of this packet, the early
            // transmit threshold, and clear OWN bit in TSD (this starts the
            // PCI operation).
            // NOTE: The length of the packet use the first 13 bits (but should
            // not exceed 1792 bytes), and a value of 0x000000 for the early
            // transmit threshold means 8 bytes. So we just write the size of
            // the packet.
            cmd_port.write(0x1FFF & len as u32);

            // When the whole packet is moved to FIFO, the OWN bit is set to 1
            while cmd_port.read() & TSR_OWN != TSR_OWN {}
            // When the whole packet is moved to line, the TOK bit is set to 1
            while cmd_port.read() & TSR_TOK != TSR_TOK {}
        }
    }

    /// Gets the next Tx buffer
    ///
    /// # Arguments
    ///
    /// * `len` - Next Tx buffer length
    fn next_tx_buffer(&mut self, len: usize) -> &mut [u8] {
        let tx_id = (self.tx_id.load(Ordering::SeqCst) + 1) % TX_BUFFERS_COUNT;
        self.tx_id.store(tx_id, Ordering::Relaxed);

        &mut self.tx_buffers[tx_id][0..len]
    }
}
