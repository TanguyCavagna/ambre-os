use alloc::vec::Vec;

use bit_field::BitField;
use lazy_static::lazy_static;
use spin::Mutex;
use x86_64::instructions::port::Port;

const CONFIG_DATA: u16 = 0xCFC;
const CONFIG_ADDRESS: u16 = 0xCF8;
const MULTI_FUNCTION_DEVICE: u8 = 0x80;

//==========================
// STRUCT / IMPL
//==========================

/// PCI configuration register
struct ConfigRegister {
    data_port: Port<u32>,
    addr_port: Port<u32>,
    addr: u32,
}

impl ConfigRegister {
    /// Creates a new configuration register
    ///
    /// # Arguments
    ///
    /// * `bus` - PCI bus number
    /// * `device` - Device number
    /// * `function` - PCI function
    /// * `offset` - Register offset
    ///
    /// see <https://wiki.osdev.org/PCI#Configuration_Space_Access_Mechanism_.231>
    pub fn new(bus: u8, device: u8, function: u8, offset: u8) -> Self {
        Self {
            data_port: Port::new(CONFIG_DATA),
            addr_port: Port::new(CONFIG_ADDRESS),
            addr: 0x8000_0000
                | ((bus as u32) << 16)
                | ((device as u32) << 11)
                | ((function as u32) << 8)
                | ((offset as u32) & 0xFC),
        }
    }

    /// Reads data from PCI
    pub fn read(&mut self) -> u32 {
        unsafe {
            self.addr_port.write(self.addr);
            self.data_port.read()
        }
    }

    /// Writes data to PCI
    ///
    /// # Arguments
    ///
    /// * `data` - Data to write
    pub fn write(&mut self, data: u32) {
        unsafe {
            self.addr_port.write(self.addr);
            self.data_port.write(data);
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub struct DeviceConfig {
    pub bus: u8,
    pub device: u8,
    pub function: u8,
    pub vendor_id: u16,
    pub device_id: u16,
    pub status: u16,
    pub command: u16,
    pub rev: u8,
    pub prog: u8,
    pub class: u8,
    pub subclass: u8,
    pub base_addresses: [u32; 6],
    pub interrupt_pin: u8,
    pub interrupt_line: u8,
}

impl DeviceConfig {
    /// Creates a new PCI configuration
    ///
    /// # Arguments
    ///
    /// * `bus` - PCI bus number
    /// * `device` - Device number
    /// * `function` - PCI function
    ///
    /// see <https://wiki.osdev.org/PCI#Header_Type_0x0>
    pub fn new(bus: u8, device: u8, function: u8) -> Self {
        let vendor_id = get_vendor_id(bus, device, function);
        let device_id = get_device_id(bus, device, function);

        let mut register = ConfigRegister::new(bus, device, function, 0x04);
        let data = register.read();
        let command = data.get_bits(0..16) as u16;
        let status = data.get_bits(16..32) as u16;

        let mut register = ConfigRegister::new(bus, device, function, 0x08);
        let data = register.read();
        let rev = data.get_bits(0..8) as u8;
        let prog = data.get_bits(8..16) as u8;
        let subclass = data.get_bits(16..24) as u8;
        let class = data.get_bits(24..32) as u8;

        let mut register = ConfigRegister::new(bus, device, function, 0x3C);
        let data = register.read();
        let interrupt_line = data.get_bits(0..8) as u8;
        let interrupt_pin = data.get_bits(8..16) as u8;

        let mut base_addresses: [u32; 6] = [0; 6];
        for i in 0..6 {
            let offset = 0x10 + ((i as u8) << 2);
            let mut register = ConfigRegister::new(bus, device, function, offset);
            base_addresses[i] = register.read();
        }

        Self {
            bus,
            device,
            function,

            // Configuration Space registers
            vendor_id,
            device_id,
            status,
            command,
            rev,
            prog,
            class,
            subclass,
            base_addresses,
            interrupt_pin,
            interrupt_line,
        }
    }

    /// Enables the bus mastering
    ///
    /// see <https://wiki.osdev.org/PCI#Command_Register>
    pub fn enable_bus_mastering(&mut self) {
        let mut register = ConfigRegister::new(self.bus, self.device, self.function, 0x04);
        let mut data = register.read();
        data.set_bit(2, true);
        register.write(data);
    }
}

//==========================
// PRIVATE
//==========================

/// Adds a device to the PCIs
///
/// # Arguments
///
/// * `bus` - PCI bus number
/// * `device` - Device number
/// * `function` - PCI function
fn add_device(bus: u8, device: u8, function: u8) {
    let config = DeviceConfig::new(bus, device, function);
    PCI_DEVICES.lock().push(config);

    log!(
        "PCI {:04X}:{:02X}:{:02X} [{:04X}:{:04X}]\n",
        bus,
        device,
        function,
        config.vendor_id,
        config.device_id
    );
}

/// Gets the vendor id
///
/// # Arguments
///
/// * `bus` - PCI bus number
/// * `device` - Device number
/// * `function` - PCI function
fn get_vendor_id(bus: u8, device: u8, function: u8) -> u16 {
    let mut register = ConfigRegister::new(bus, device, function, 0x00);
    register.read().get_bits(0..16) as u16
}

/// Gets the device id
///
/// # Arguments
///
/// * `bus` - PCI bus number
/// * `device` - Device number
/// * `function` - PCI function
fn get_device_id(bus: u8, device: u8, function: u8) -> u16 {
    let mut register = ConfigRegister::new(bus, device, function, 0x00);
    register.read().get_bits(16..32) as u16
}

/// Gets the header type
///
/// # Arguments
///
/// * `bus` - PCI bus number
/// * `device` - Device number
/// * `function` - PCI function
fn get_header_type(bus: u8, device: u8, function: u8) -> u8 {
    let mut register = ConfigRegister::new(bus, device, function, 0x0C);
    register.read().get_bits(16..24) as u8
}

/// Check the given device on the given port and add it to the devices if it exists
///
/// # Arguments
///
/// * `bus` - PCI bus number
/// * `device` - Device number
fn check_device(bus: u8, device: u8) {
    let function = 0;

    let vendor_id = get_vendor_id(bus, device, function);
    if vendor_id == 0xFFFF {
        return; // Device doesn't exist
    }

    add_device(bus, device, function);

    // Multi-function devices
    let header_type = get_header_type(bus, device, function);

    if header_type & MULTI_FUNCTION_DEVICE != 0 {
        for function in 1..8 {
            let vendor_id = get_vendor_id(bus, device, function);

            if vendor_id != 0xFFFF {
                add_device(bus, device, function);
            }
        }
    }
}

/// Check all the possible devices
///
/// # Arguments
///
/// * `bus` - PCI bus number
fn check_bus(bus: u8) {
    for device in 0..32 {
        check_device(bus, device);
    }
}

//==========================
// PUBLIC
//==========================

lazy_static! {
    pub static ref PCI_DEVICES: Mutex<Vec<DeviceConfig>> = Mutex::new(Vec::new());
}

/// Lists all PCI devices
pub fn list() -> Vec<DeviceConfig> { PCI_DEVICES.lock().clone() }

/// Finds a device
///
/// # Arguments
///
/// * `vendor_id` - Vendor id of the wanted device
/// * `device_id` - Device number of the wanted devide
pub fn find_device(vendor_id: u16, device_id: u16) -> Option<DeviceConfig> {
    for &device in PCI_DEVICES.lock().iter() {
        if device.vendor_id == vendor_id && device.device_id == device_id {
            return Some(device);
        }
    }
    None
}

pub fn init() {
    for bus in 0..256 {
        check_bus(bus as u8);
    }

    let devs = PCI_DEVICES.lock();
    for dev in devs.iter() {
        // NOTE: There's not yet an AHCI driver for SATA disks so we must
        // switch to IDE legacy mode for the ATA driver.
        if dev.class != 0x01 || dev.subclass != 0x01 {
            continue;
        }

        // IDE Controller
        let mut register = ConfigRegister::new(dev.bus, dev.device, dev.function, 0x08);
        let mut data = register.read();
        let prog_offset = 8; // The programing interface start at bit 8

        // Switching primary channel to compatibility mode
        if !dev.prog.get_bit(0) {
            continue;
        }

        // PCI native mode
        if !dev.prog.get_bit(1) {
            continue;
        }

        // Modifiable
        data.set_bit(prog_offset, false);
        register.write(data);

        // Switching secondary channel to compatibility mode
        if !dev.prog.get_bit(2) {
            continue;
        }

        // PCI native mode
        if !dev.prog.get_bit(3) {
            continue;
        }

        // Modifiable
        data.set_bit(prog_offset + 2, false);
        register.write(data);
    }
}
