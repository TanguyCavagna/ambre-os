use pic8259::ChainedPics;
use spin::Mutex;

use crate::println;

// Custom interrupt range is set because the default range 0x0-0xF is reserved for the CPU exception like PageFault or DoubleFault.
// We choose the range 0x20-0x2F which is the typicall range for interrupts.
pub const PIC_1_OFFSET: u8 = 32;
pub const PIC_2_OFFSET: u8 = PIC_1_OFFSET + 8;

/// Setup Programmable Interrupt Controller
///
/// Controller used to handle all interrupt that are not triggered by the CPU.
/// E.g.: Keyboard, timers, mouse, serial port, etc.
pub static PICS: Mutex<ChainedPics> =
    Mutex::new(unsafe { ChainedPics::new(PIC_1_OFFSET, PIC_2_OFFSET) });

pub fn init() {
    if !cfg!(feature = "quiet") {
        log!("Initializing PIC... ");
    }

    unsafe {
        PICS.lock().initialize();
    }

    x86_64::instructions::interrupts::enable();

    if !cfg!(feature = "quiet") {
        let csi_color = crate::sys::console::Style::color("LightGreen");
        let csi_reset = crate::sys::console::Style::reset();
        println!("{}[ok]{}", csi_color, csi_reset);
    }
}
