use alloc::{
    boxed::Box,
    collections::BTreeMap,
    string::{String, ToString},
    vec::Vec,
};
use core::{
    arch::asm,
    sync::atomic::{AtomicUsize, Ordering},
};

use lazy_static::lazy_static;
use object::{Object, ObjectSegment};
use spin::RwLock;

use super::{
    console::Console,
    fs::{Device, Resource},
};

const MAX_FILE_HANDLES: usize = 64;
const MAX_PROCS: usize = 2; // TODO: Update this when more than one process can run at once
const MAX_PROC_SIZE: usize = 1 << 20; // 1 MB

pub static PID: AtomicUsize = AtomicUsize::new(0);
pub static MAX_PID: AtomicUsize = AtomicUsize::new(1);

//==========================
// STRUCT / IMPL
//==========================

#[derive(Clone, Debug)]
pub struct ProcessData {
    env: BTreeMap<String, String>,
    dir: String,
    user: Option<String>,
    file_handles: [Option<Box<Resource>>; MAX_FILE_HANDLES],
}

impl ProcessData {
    pub fn new(dir: &str, user: Option<&str>) -> Self {
        let env = BTreeMap::new();
        let dir = dir.to_string();
        let user = user.map(String::from);
        let mut file_handles = [(); MAX_FILE_HANDLES].map(|_| None);

        file_handles[0] = Some(Box::new(Resource::Device(Device::Console(Console::new()))));
        file_handles[1] = Some(Box::new(Resource::Device(Device::Console(Console::new()))));
        file_handles[2] = Some(Box::new(Resource::Device(Device::Console(Console::new()))));
        file_handles[3] = Some(Box::new(Resource::Device(Device::Null)));

        Self {
            env,
            dir,
            user,
            file_handles,
        }
    }
}

//====================================================
// USERSPACE EXPERIMENT
//====================================================

// See https://nfil.dev/kernel/rust/coding/rust-kernel-to-userspace-and-back/
// And https://github.com/WartaPoirier-corp/ananos/blob/dev/docs/notes/context-switch.md

use core::sync::atomic::AtomicU64;

use x86_64::{structures::idt::InterruptStackFrameValue, VirtAddr};

use crate::sys::gdt::GDT;
use crate::{api::process::ExitCode, sys};

static CODE_ADDR: AtomicU64 = AtomicU64::new((sys::allocator::HEAP_START as u64) + (16 << 20));

const ELF_MAGIC: [u8; 4] = [0x7F, b'E', b'L', b'F'];
const BIN_MAGIC: [u8; 4] = [0x7F, b'B', b'I', b'N'];

#[repr(align(8), C)]
#[derive(Debug, Clone, Copy, Default)]
pub struct Registers {
    pub r11: usize,
    pub r10: usize,
    pub r9: usize,
    pub r8: usize,
    pub rdi: usize,
    pub rsi: usize,
    pub rdx: usize,
    pub rcx: usize,
    pub rax: usize,
}

#[derive(Clone, Debug)]
pub struct Process {
    id: usize,
    code_addr: u64,
    stack_addr: u64,
    entry_point: u64,
    stack_frame: InterruptStackFrameValue,
    registers: Registers,
    data: ProcessData,
}

impl Process {
    pub fn new(id: usize) -> Self {
        let isf = InterruptStackFrameValue {
            instruction_pointer: VirtAddr::new(0),
            code_segment: 0,
            cpu_flags: 0,
            stack_pointer: VirtAddr::new(0),
            stack_segment: 0,
        };
        Self {
            id,
            code_addr: 0,
            stack_addr: 0,
            entry_point: 0,
            stack_frame: isf,
            registers: Registers::default(),
            data: ProcessData::new("/", None),
        }
    }

    /// Spawns a new process
    ///
    /// # Arguments
    ///
    /// * `bin` - Binary
    /// * `args_ptr` - Arguments pointer
    /// * `args_len` - Arguments length
    pub fn spawn(bin: &[u8], args_ptr: usize, args_len: usize) -> Result<(), ExitCode> {
        if let Ok(pid) = Self::create(bin) {
            let proc = {
                let table = PROCESS_TABLE.read();
                table[pid].clone()
            };

            proc.exec(args_ptr, args_len);

            Ok(())
        } else {
            Err(ExitCode::ExecError)
        }
    }

    /// Creates a new process
    ///
    /// # Arguments
    ///
    /// * `bin` - Binary
    fn create(bin: &[u8]) -> Result<usize, ()> {
        let proc_size = MAX_PROC_SIZE as u64;
        let code_addr = CODE_ADDR.fetch_add(proc_size, Ordering::SeqCst);
        let stack_addr = code_addr + proc_size;

        let mut entry_point = 0;
        let code_ptr = code_addr as *mut u8;

        if bin[0..4] == ELF_MAGIC {
            // ELF binary
            if let Ok(obj) = object::File::parse(bin) {
                entry_point = obj.entry();

                for segment in obj.segments() {
                    let addr = segment.address() as usize;

                    if let Ok(data) = segment.data() {
                        for (i, b) in data.iter().enumerate() {
                            unsafe { core::ptr::write(code_ptr.add(addr + i), *b) };
                        }
                    }
                }
            }
        } else if bin[0..4] == BIN_MAGIC {
            // Flat binary
            for (i, b) in bin.iter().skip(4).enumerate() {
                unsafe { core::ptr::write(code_ptr.add(i), *b) };
            }
        } else {
            return Err(());
        }

        let mut table = PROCESS_TABLE.write();
        let parent = &table[id()];

        let data = parent.data.clone();
        let registers = parent.registers;
        let stack_frame = parent.stack_frame;

        let id = MAX_PID.fetch_add(1, Ordering::SeqCst);
        let proc = Process {
            id,
            code_addr,
            stack_addr,
            entry_point,
            data,
            stack_frame,
            registers,
        };
        table[id] = Box::new(proc);

        Ok(id)
    }

    /// Executes the program in user mode
    ///
    /// # Arguments
    ///
    /// * `args_ptr` - Arguments pointer
    /// * `args_len` - Arguments length
    fn exec(&self, args_ptr: usize, args_len: usize) {
        let heap_addr = self.code_addr + (self.stack_addr - self.code_addr) / 2;
        sys::allocator::alloc_pages(heap_addr, 1);

        let args_ptr = ptr_from_addr(args_ptr as u64) as usize;
        let args: &[&str] =
            unsafe { core::slice::from_raw_parts(args_ptr as *const &str, args_len) };
        let mut addr = heap_addr;
        let vec: Vec<&str> = args
            .iter()
            .map(|arg| {
                let ptr = addr as *mut u8;
                addr += arg.len() as u64;
                unsafe {
                    let s = core::slice::from_raw_parts_mut(ptr, arg.len());
                    s.copy_from_slice(arg.as_bytes());
                    core::str::from_utf8_unchecked(s)
                }
            })
            .collect();
        let align = core::mem::align_of::<&str>() as u64;

        addr += align - (addr % align);

        let args = vec.as_slice();
        let ptr = addr as *mut &str;
        let args: &[&str] = unsafe {
            let s = core::slice::from_raw_parts_mut(ptr, args.len());
            s.copy_from_slice(args);
            s
        };
        let args_ptr = args.as_ptr() as u64;

        set_id(self.id); // Change PID
        unsafe {
            asm!(
                "cli",        // Disable interrupts
                "push {:r}",  // Stack segment (SS)
                "push {:r}",  // Stack pointer (RSP)
                "push 0x200", // RFLAGS with interrupts enabled
                "push {:r}",  // Code segment (CS)
                "push {:r}",  // Instruction pointer (RIP)
                "iretq",
                in(reg) GDT.1.user_data.0,
                in(reg) self.stack_addr,
                in(reg) GDT.1.user_code.0,
                in(reg) self.code_addr + self.entry_point,
                in("rdi") args_ptr,
                in("rsi") args_len,
            );
        }
    }
}

//==========================
// PUBLIC
//==========================

lazy_static! {
    pub static ref PROCESS_TABLE: RwLock<[Box<Process>; MAX_PROCS]> =
        RwLock::new([(); MAX_PROCS].map(|_| Box::new(Process::new(0))));
}

/// Gets the ID
pub fn id() -> usize { PID.load(Ordering::SeqCst) }

/// Sets the ID
pub fn set_id(id: usize) { PID.store(id, Ordering::SeqCst) }

/// Gets an environement variable
///
/// # Arguments
///
/// * `key` - Env key
pub fn env(key: &str) -> Option<String> {
    let table = PROCESS_TABLE.read();
    let proc = &table[id()];

    proc.data.env.get(key).cloned()
}

/// Gets all environement variables
pub fn envs() -> BTreeMap<String, String> {
    let table = PROCESS_TABLE.read();
    let proc = &table[id()];

    proc.data.env.clone()
}

/// Gets the process directory
pub fn dir() -> String {
    let table = PROCESS_TABLE.read();
    let proc = &table[id()];

    proc.data.dir.clone()
}

/// Gets the user
pub fn user() -> Option<String> {
    let table = PROCESS_TABLE.read();
    let proc = &table[id()];

    proc.data.user.clone()
}

/// Sets an environement variable
///
/// # Arguments
///
/// * `key` - Env key
pub fn set_env(key: &str, val: &str) {
    let mut table = PROCESS_TABLE.write();
    let proc = &mut table[id()];

    proc.data.env.insert(key.into(), val.into());
}

/// Sets a process directory
///
/// # Arguments
///
/// * `dir` - New process directory
pub fn set_dir(dir: &str) {
    let mut table = PROCESS_TABLE.write();
    let proc = &mut table[id()];

    proc.data.dir = dir.into();
}

/// Sets a process user
///
/// # Arguments
///
/// * `user` - User
pub fn set_user(user: &str) {
    let mut table = PROCESS_TABLE.write();
    let proc = &mut table[id()];

    proc.data.user = Some(user.into())
}

/// Creates a file handler
///
/// # Arguments
///
/// * `file` - File to handle
pub fn create_file_handle(file: Resource) -> Result<usize, ()> {
    let mut table = PROCESS_TABLE.write();
    let proc = &mut table[id()];
    let min = 4; // The first 4 file handles are reserved
    let max = MAX_FILE_HANDLES;

    for handle in min..max {
        if proc.data.file_handles[handle].is_none() {
            proc.data.file_handles[handle] = Some(Box::new(file));

            return Ok(handle);
        }
    }

    debug!("Could not create file handle");

    Err(())
}

/// Update the file handler
///
/// # Arguments
///
/// * `handle` - File handler
pub fn update_file_handle(handle: usize, file: Resource) {
    let mut table = PROCESS_TABLE.write();
    let proc = &mut table[id()];

    proc.data.file_handles[handle] = Some(Box::new(file));
}

/// Deletes a file handler
///
/// # Arguments
///
/// * `handle` - File handler
pub fn delete_file_handle(handle: usize) {
    let mut table = PROCESS_TABLE.write();
    let proc = &mut table[id()];

    proc.data.file_handles[handle] = None;
}

/// Gets a file handler
///
/// # Arguments
///
/// * `handle` - Handler to get
pub fn file_handle(handle: usize) -> Option<Box<Resource>> {
    let table = PROCESS_TABLE.read();
    let proc = &table[id()];

    proc.data.file_handles[handle].clone()
}

/// Gets all file handlers
pub fn file_handles() -> Vec<Option<Box<Resource>>> {
    let table = PROCESS_TABLE.read();
    let proc = &table[id()];

    proc.data.file_handles.to_vec()
}

/// Get code address
pub fn code_addr() -> u64 {
    let table = PROCESS_TABLE.read();
    let proc = &table[id()];

    proc.code_addr
}

/// Sets code address
///
/// # Arguments
///
/// * `addr` - New code address
pub fn set_code_addr(addr: u64) {
    let mut table = PROCESS_TABLE.write();
    let mut proc = &mut table[id()];

    proc.code_addr = addr;
}

/// Converts an address into a pointer
///
/// # Arguments
///
/// * `addr` - Address to convert
pub fn ptr_from_addr(addr: u64) -> *mut u8 {
    let base = code_addr();

    if addr < base {
        (base + addr) as *mut u8
    } else {
        addr as *mut u8
    }
}

/// Gets all registers
pub fn registers() -> Registers {
    let table = PROCESS_TABLE.read();
    let proc = &table[id()];

    proc.registers
}

/// Sets all registers
///
/// # Arguments
///
/// * `regs` - Registers
pub fn set_registers(regs: Registers) {
    let mut table = PROCESS_TABLE.write();
    let mut proc = &mut table[id()];

    proc.registers = regs
}

/// Get the stack frame
pub fn stack_frame() -> InterruptStackFrameValue {
    let table = PROCESS_TABLE.read();
    let proc = &table[id()];

    proc.stack_frame
}

/// Set the stack frame
///
/// # Arguments
///
/// * `stack_frame` - New stack frame
pub fn set_stack_frame(stack_frame: InterruptStackFrameValue) {
    let mut table = PROCESS_TABLE.write();
    let mut proc = &mut table[id()];

    proc.stack_frame = stack_frame;
}

/// Exists a process
pub fn exit() {
    let table = PROCESS_TABLE.read();
    let proc = &table[id()];

    sys::allocator::free_pages(proc.code_addr, MAX_PROC_SIZE);
    MAX_PID.fetch_sub(1, Ordering::SeqCst);
    set_id(0); // FIXME: No process manager so we switch back to process 0
}
