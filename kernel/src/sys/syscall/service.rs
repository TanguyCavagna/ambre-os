use alloc::vec;
use core::arch::asm;

use crate::api::process::ExitCode;
use crate::sys;
use crate::sys::fs::FileIO;
use crate::sys::fs::FileInfo;
use crate::sys::process::Process;

//==========================
// PUBLIC
//==========================

/// Exits
///
/// # Arguments
///
/// * `code` - Exit code
pub fn exit(code: ExitCode) -> ExitCode {
    sys::process::exit();

    code
}

/// Sleeps
///
/// # Arguments
///
/// * `seconds` - Seconds to sleep
pub fn sleep(seconds: f64) { sys::time::sleep(seconds); }

/// Deletes a path
///
/// # Arguments
///
/// * `path` - Path to delete
pub fn delete(path: &str) -> isize {
    if sys::fs::delete(path).is_ok() {
        0
    } else {
        -1
    }
}

/// Sets the FileInfo of a given path
///
/// # Arguments
///
/// * `path` - Entry path
/// * `info` - Info to write
pub fn info(path: &str, info: &mut FileInfo) -> isize {
    let path = match sys::fs::canonicalize(path) {
        Ok(path) => path,
        Err(_) => return -1,
    };
    if let Some(res) = sys::fs::info(&path) {
        *info = res;

        0
    } else {
        -1
    }
}

/// Opens an entry
///
/// # Arguments
///
/// * `path` - Entry path
/// * `flags` - Flags
pub fn open(path: &str, flags: usize) -> isize {
    let path = match sys::fs::canonicalize(path) {
        Ok(path) => path,
        Err(_) => return -1,
    };
    if let Some(resource) = sys::fs::open(&path, flags) {
        if let Ok(handle) = sys::process::create_file_handle(resource) {
            return handle as isize;
        }
    }

    -1
}

/// Duplicate a file handler
///
/// # Arguments
///
/// * `old_handle` - Old handler
/// * `new_handle` - New handler
pub fn dup(old_handle: usize, new_handle: usize) -> isize {
    if let Some(file) = sys::process::file_handle(old_handle) {
        sys::process::update_file_handle(new_handle, *file);

        return new_handle as isize;
    }

    -1
}

/// Reads a file into a buffer
///
/// # Arguments
///
/// * `handle` - File handler
/// * `buf` - Buffer to write into
pub fn read(handle: usize, buf: &mut [u8]) -> isize {
    if let Some(mut file) = sys::process::file_handle(handle) {
        if let Ok(bytes) = file.read(buf) {
            sys::process::update_file_handle(handle, *file);

            return bytes as isize;
        }
    }

    -1
}

/// Write a file from a buffer
///
/// # Arguments
///
/// * `handle` - File handler
/// * `buf` - Buffer to read from
pub fn write(handle: usize, buf: &mut [u8]) -> isize {
    if let Some(mut file) = sys::process::file_handle(handle) {
        if let Ok(bytes) = file.write(buf) {
            sys::process::update_file_handle(handle, *file);

            return bytes as isize;
        }
    }

    -1
}

/// Closes a file
///
/// # Arguments
///
/// * `handle` - File handler
pub fn close(handle: usize) { sys::process::delete_file_handle(handle); }

/// Spawns a process
///
/// # Arguments
///
/// * `path` - File path
/// * `args_ptr` - Arguments pointer
/// * `args_len` - Arguments length
pub fn spawn(path: &str, args_ptr: usize, args_len: usize) -> ExitCode {
    let path = match sys::fs::canonicalize(path) {
        Ok(path) => path,
        Err(_) => return ExitCode::OpenError,
    };

    if let Some(mut file) = sys::fs::File::open(&path) {
        let mut buf = vec![0; file.size()];

        if let Ok(bytes) = file.read(&mut buf) {
            buf.resize(bytes, 0);

            if let Err(code) = Process::spawn(&buf, args_ptr, args_len) {
                code
            } else {
                ExitCode::Success
            }
        } else {
            ExitCode::ReadError
        }
    } else {
        ExitCode::OpenError
    }
}

/// Stops
///
/// # Arguments
///
/// * `code` - Stop code
pub fn stop(code: usize) -> usize {
    match code {
        0xcafe => {
            // Reboot
            unsafe {
                asm!("xor rax, rax", "mov cr3, rax");
            }
        }
        0xdead => {
            // Halt
            // sys::acpi::shutdown();
        }
        _ => {
            debug!("STOP SYSCALL: Invalid code '{:#x}' received", code);
        }
    }

    0
}
