use core::{
    hint::spin_loop,
    sync::atomic::{AtomicU64, AtomicUsize, Ordering},
};

use x86_64::instructions::{interrupts, port::Port};

use super::cmos::CMOS;
use crate::{println, sys};

// Programmable Interval Timer frequency
// see <https://wiki.osdev.org/Programmable_Interval_Timer#The_Oscillator>
pub const PIT_FREQUENCY: f64 = 3_579_545.0 / 3.0; // 1_193_181.666 Hz

// At boot the PIT starts with a frequency divider of 0 (equivalent to 65536)
// which will result in about 54.926 ms between ticks.
// During init we will change the divider to 1193 to have about 1.000 ms
// between ticks to improve time measurements accuracy.
const PIT_DIVIDER: usize = 1193;
const PIT_INTERVAL: f64 = (PIT_DIVIDER as f64) / PIT_FREQUENCY;
const PIT_COMMAND_REGISTER: u16 = 0x43;
const PIT_CHANNEL_OFFSET: u16 = 0x40;
const PIT_MODE_WAVE_FORM: u8 = 0x03;

// PIT I/O port see <https://wiki.osdev.org/Programmable_Interval_Timer#I.2FO_Ports>
const PIT_ACCESS_MODE_LOBYTE: u8 = 0b01;
const PIT_ACCESS_MODE_HIBYTE: u8 = 0b10;

static PIT_TICKS: AtomicUsize = AtomicUsize::new(0);
static LAST_RTC_UPATE: AtomicUsize = AtomicUsize::new(0);
static CLOCKS_PER_NANOSECOND: AtomicU64 = AtomicU64::new(0);

/// Gets how many ticks as passed
pub fn ticks() -> usize { PIT_TICKS.load(Ordering::Relaxed) }

/// Gets the time between two ticks
pub fn time_between_ticks() -> f64 { PIT_INTERVAL }

/// Gets the last RTC update time
pub fn last_rtc_update() -> usize { LAST_RTC_UPATE.load(Ordering::Relaxed) }

/// Halts the CPU
pub fn halt() {
    let int_disabled = !interrupts::are_enabled();

    interrupts::enable_and_hlt(); // enable interrupts and halt them

    // force disable interrupts
    if int_disabled {
        interrupts::disable();
    }
}

/// Reads the timestamp counter
pub fn rdtsc() -> u64 {
    unsafe {
        // assure that no other instructions will corrupt the rdtsc. See <https://www.felixcloutier.com/x86/lfence>
        core::arch::x86_64::_mm_lfence();
        core::arch::x86_64::_rdtsc()
    }
}

/// Sleeps the given amount of seconds
///
/// # Arguments
///
/// * `seconds` - Time to sleep
pub fn sleep(seconds: f64) {
    let start = sys::clock::uptime();

    while sys::clock::uptime() - start < seconds {
        halt();
    }
}

/// Waits some nanoseconds
///
/// # Arguments
///
/// * `nanoseconds` - Nanoseconds to wait
pub fn nanowait(nanoseconds: u64) {
    let start = rdtsc();
    let delta = nanoseconds * CLOCKS_PER_NANOSECOND.load(Ordering::Relaxed);

    while rdtsc() - start < delta {
        spin_loop();
    }
}

/// Sets the PIT divider
///
/// # Arguments
///
/// * `divider` - Frequency divider must be in range 0-65535, with 0 acting as 65536
/// * `channel` - Can be the following. 0 for PIT chip, 1 no longer used, 2 PC speaker
pub fn set_pit_frequency_divider(divider: u16, channel: u8) {
    interrupts::without_interrupts(|| {
        let bytes = divider.to_le_bytes();
        let mut cmd: Port<u8> = Port::new(PIT_COMMAND_REGISTER);
        let mut data: Port<u8> = Port::new(PIT_CHANNEL_OFFSET + channel as u16);
        let operating_mode = PIT_MODE_WAVE_FORM;
        let access_mode = PIT_ACCESS_MODE_LOBYTE | PIT_ACCESS_MODE_HIBYTE;

        unsafe {
            cmd.write((channel << 6) | (access_mode << 4) | (operating_mode << 1) | 0);
            data.write(bytes[0]);
            data.write(bytes[1]);
        }
    });
}

/// Handles the PIT interrupts
pub fn pit_interrupt_handler() { PIT_TICKS.fetch_add(1, Ordering::Relaxed); }

/// Handles the RTC interrutps
pub fn rtc_interrupt_handler() {
    LAST_RTC_UPATE.store(ticks(), Ordering::Relaxed);
    CMOS::new().notify_end_of_interrupt();
}

pub fn init() {
    if !cfg!(feature = "quiet") {
        log!("Initializing Time... ");
    }

    // PIT timer
    let divider = if PIT_DIVIDER < 65536 { PIT_DIVIDER } else { 0 };
    let channel = 0;

    set_pit_frequency_divider(divider as u16, channel);
    sys::idt::set_irq_handler(
        sys::idt::InterruptIndex::Timer.as_index(),
        pit_interrupt_handler,
    );

    // RTC timer
    sys::idt::set_irq_handler(
        sys::idt::InterruptIndex::RTC.as_index(),
        rtc_interrupt_handler,
    );
    CMOS::new().enable_update_interrupt();

    // TSC timer
    let calibration_time = 250_000; // 0.25 seconds
    let a = rdtsc();
    sleep(calibration_time as f64 / 1e6);
    let b = rdtsc();
    CLOCKS_PER_NANOSECOND.store((b - a) / calibration_time, Ordering::Relaxed);

    if !cfg!(feature = "quiet") {
        let csi_color = crate::sys::console::Style::color("LightGreen");
        let csi_reset = crate::sys::console::Style::reset();
        println!("{}[ok]{}", csi_color, csi_reset);
    }
}
