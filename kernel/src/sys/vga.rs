//! VGA Buffer is used to display text onto a scren. It uses 2 bytes per character. The first one
//! is "graphical" representation. Background and foreground, and the second one is the
//! the character itself (not quite ASCII but close enough).
//!
//! **see :** <https://en.wikipedia.org/wiki/VGA_text_mode>

use core::fmt::{self, Write};

use bit_field::BitField;
use lazy_static::lazy_static;
use spin::Mutex;
use vte::{Params, Parser, Perform};
use x86_64::instructions::interrupts;
use x86_64::instructions::port::Port;

use crate::api::vga::color;
use crate::api::vga::{Color, Palette};
use crate::sys;

// See <https://web.stanford.edu/class/cs140/projects/pintos/specs/freevga/vga/vga.htm>
// And <https://01.org/sites/default/files/documentation/snb_ihd_os_vol3_part1_0.pdf>

const ATTR_ADDR_DATA_REG: u16 = 0x3C0;
const ATTR_DATA_READ_REG: u16 = 0x3C1;
#[allow(dead_code)]
const SEQUENCER_ADDR_REG: u16 = 0x3C4;
const DAC_ADDR_WRITE_MODE_REG: u16 = 0x3C8;
#[allow(dead_code)]
const DAC_DATA_REG: u16 = 0x3C9;
#[allow(dead_code)]
const GRAPHICS_ADDR_REG: u16 = 0x3CE;
const CRTC_ADDR_REG: u16 = 0x3D4;
const CRTC_DATA_REG: u16 = 0x3D5;
const INPUT_STATUS_REG: u16 = 0x3DA;

const FG: Color = Color::LightGray;
const BG: Color = Color::Black;
const UNPRINTABLE: u8 = 0x00; // Unprintable chars will be replaced by this one
const MAXIMUM_PALETTE_COLORS: usize = 16;

//==========================
// ENUM / STRUCT
//==========================

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(transparent)]
struct ColorCode(u8);

impl ColorCode {
    fn new(foreground: Color, background: Color) -> ColorCode {
        ColorCode((background as u8) << 4 | (foreground as u8))
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(C)]
struct ScreenChar {
    ascii_code: u8,
    color_code: ColorCode,
}

const BUFFER_HEIGHT: usize = 25;
const BUFFER_WIDTH: usize = 80;

#[repr(transparent)]
struct Buffer {
    chars: [[ScreenChar; BUFFER_WIDTH]; BUFFER_HEIGHT],
}

pub struct Writer {
    cursor: [usize; 2], // x, y
    writer: [usize; 2], // x, y
    color_code: ColorCode,
    buffer: &'static mut Buffer,
}

impl Writer {
    /// Gets the current wirter position
    fn writer_position(&self) -> (usize, usize) { (self.writer[0], self.writer[1]) }

    /// Sets the writer position
    fn set_writer_position(&mut self, x: usize, y: usize) { self.writer = [x, y]; }

    /// Gets the cursor position
    fn cursor_position(&self) -> (usize, usize) { (self.cursor[0], self.cursor[1]) }

    /// Set the cursors position
    fn set_cursor_position(&mut self, x: usize, y: usize) {
        self.cursor = [x, y];
        self.write_cursor();
    }

    /// Write at cursor position
    fn write_cursor(&mut self) {
        let pos = self.cursor[0] + self.cursor[1] * BUFFER_WIDTH;
        let mut addr = Port::new(CRTC_ADDR_REG);
        let mut data = Port::new(CRTC_DATA_REG);
        unsafe {
            addr.write(0x0F as u8);
            data.write((pos & 0xFF) as u8);
            addr.write(0x0E as u8);
            data.write(((pos >> 8) & 0xFF) as u8);
        }
    }

    /// Disables the cursor
    fn disable_cursor(&self) {
        // <http://www.osdever.net/FreeVGA/vga/crtcreg.htm#0A>
        let mut addr = Port::new(CRTC_ADDR_REG);
        let mut data = Port::new(CRTC_DATA_REG);
        unsafe {
            addr.write(0x0A as u8);
            data.write(0x20 as u8);
        }
    }

    /// Enables the cursor
    fn enable_cursor(&self) {
        let mut addr: Port<u8> = Port::new(CRTC_ADDR_REG);
        let mut data: Port<u8> = Port::new(CRTC_DATA_REG);
        let cursor_start = 13; // Starting row
        let cursor_end = 14; // Ending row
        unsafe {
            addr.write(0x0A); // Cursor Start Register
            let b = data.read();
            data.write((b & 0xC0) | cursor_start);

            addr.write(0x0B); // Cursor End Register
            let b = data.read();
            data.write((b & 0xE0) | cursor_end);
        }
    }

    /// Disables echo
    fn disable_echo(&self) { sys::console::disable_echo(); }

    /// Enables echo
    fn enable_echo(&self) { sys::console::enable_echo(); }

    /// Write a given byte
    ///
    /// # Arguments
    ///
    /// * `byte` - Byte to write
    fn write_byte(&mut self, byte: u8) {
        match byte {
            // newline
            0x0A => {
                self.new_line();
            }
            // carriage Return
            0x0D => {}
            // backspace
            0x08 => {
                if self.writer[0] <= 0 {
                    return;
                }

                // Put the wirter one char behind current pos and replace char by a whitespace
                self.writer[0] -= 1;
                let c = ScreenChar {
                    ascii_code: b' ',
                    color_code: self.color_code,
                };
                let x = self.writer[0];
                let y = self.writer[1];

                // write
                unsafe {
                    core::ptr::write_volatile(&mut self.buffer.chars[y][x], c);
                }
            }
            // any other byte
            byte => {
                if self.writer[0] >= BUFFER_WIDTH {
                    self.new_line();
                }

                // get the current writer position and print the byte at this pos
                let x = self.writer[0];
                let y = self.writer[1];
                let ascii_code = if is_printable(byte) {
                    byte
                } else {
                    UNPRINTABLE
                };
                let color_code = self.color_code;
                let c = ScreenChar {
                    ascii_code,
                    color_code,
                };

                // write
                unsafe {
                    core::ptr::write_volatile(&mut self.buffer.chars[y][x], c);
                }

                // move the writer to next pos on the line
                self.writer[0] += 1;
            }
        }
    }

    /// Adds a newline to the writer
    fn new_line(&mut self) {
        // Not the last line
        if self.writer[1] < BUFFER_HEIGHT - 1 {
            self.writer[1] += 1;
        }
        // Last line, shift all lines up by 1
        else {
            for y in 1..BUFFER_HEIGHT {
                for x in 0..BUFFER_WIDTH {
                    unsafe {
                        let c = core::ptr::read_volatile(&self.buffer.chars[y][x]);
                        core::ptr::write_volatile(&mut self.buffer.chars[y - 1][x], c);
                    }
                }
            }
            self.clear_row_after(0, BUFFER_HEIGHT - 1);
        }
        self.writer[0] = 0;
    }

    /// Clears the given row after the given col
    ///
    /// # Arguments
    ///
    /// * `x` - Writer column
    /// * `y` - Writer row
    fn clear_row_after(&mut self, x: usize, y: usize) {
        let empty_char = ScreenChar {
            ascii_code: b' ',
            color_code: self.color_code,
        };

        for i in x..BUFFER_WIDTH {
            unsafe {
                core::ptr::write_volatile(&mut self.buffer.chars[y][i], empty_char);
            }
        }
    }

    /// Clears the whole writer
    fn clear_screen(&mut self) {
        for y in 0..BUFFER_HEIGHT {
            self.clear_row_after(0, y);
        }
    }

    /// Sets the writer color
    ///
    /// # Arguments
    ///
    /// * `foreground` - Foreground color
    /// * `background` - Background color
    pub fn set_color(&mut self, foreground: Color, background: Color) {
        self.color_code = ColorCode::new(foreground, background);
    }

    /// Gets the current writer color
    pub fn color(&self) -> (Color, Color) {
        let cc = self.color_code.0;
        let fg = color::from_index(cc.get_bits(0..4) as usize);
        let bg = color::from_index(cc.get_bits(4..8) as usize);

        (fg, bg)
    }

    /// Sets the wirter palette
    ///
    /// # Arguments
    ///
    /// * `palette` - VGA custom palette
    pub fn set_palette(&mut self, palette: Palette) {
        let mut addr: Port<u8> = Port::new(DAC_ADDR_WRITE_MODE_REG);
        let mut data: Port<u8> = Port::new(DAC_DATA_REG);

        for (i, (r, g, b)) in palette.colors.iter().enumerate() {
            if i < MAXIMUM_PALETTE_COLORS {
                let reg = color::from_index(i as usize).to_vga_reg();
                unsafe {
                    addr.write(reg);
                    data.write(vga_color(*r));
                    data.write(vga_color(*g));
                    data.write(vga_color(*b));
                }
            }
        }
    }
}

impl fmt::Write for Writer {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        let mut parser = PARSER.lock();
        for byte in s.bytes() {
            parser.advance(self, byte);
        }
        let (x, y) = self.writer_position();
        self.set_cursor_position(x, y);
        Ok(())
    }
}

impl Perform for Writer {
    fn print(&mut self, c: char) { self.write_byte(c as u8) }

    fn execute(&mut self, byte: u8) { self.write_byte(byte) }

    // see <https://wiki.osdev.org/Terminals#Sending_Sequences>
    // still don't figured out how to use thoses sequences...
    fn csi_dispatch(&mut self, params: &Params, _: &[u8], _: bool, action: char) {
        match action {
            // reset color attributes
            'm' => {
                let mut fg = FG;
                let mut bg = BG;

                // verify if some colors have been passed with the command
                for param in params.iter() {
                    match param[0] {
                        0 => {
                            fg = FG;
                            bg = BG;
                        }
                        // Black to LightGrey or DarGrey to White
                        30..=37 | 90..=97 => {
                            fg = color::from_ansi(param[0] as u8);
                        }
                        // Black to LightGrey or DarGrey to White but for background
                        40..=47 | 100..=107 => {
                            bg = color::from_ansi((param[0] as u8) - 10);
                        }
                        _ => {}
                    }
                }

                self.set_color(fg, bg);
            }
            // Cursor Up
            'A' => {
                let mut n = 1;

                for param in params.iter() {
                    n = param[0] as usize;
                }

                self.writer[1] -= if self.writer[1] == 1 { 0 } else { n };
                self.cursor[1] -= if self.cursor[1] == 1 { 0 } else { n };
            }
            // Cursor Down
            'B' => {
                let mut n = 1;

                for param in params.iter() {
                    n = param[0] as usize;
                }

                self.writer[1] += if (self.writer[1] + n) > BUFFER_HEIGHT {
                    BUFFER_HEIGHT
                } else {
                    n
                };
                self.cursor[1] += if (self.cursor[1] + n) > BUFFER_HEIGHT {
                    BUFFER_HEIGHT
                } else {
                    n
                };
            }
            // Cursor Forward
            'C' => {
                let mut n = 1;

                for param in params.iter() {
                    n = param[0] as usize;
                }

                self.writer[0] += if (self.writer[0] + n) > BUFFER_WIDTH {
                    BUFFER_WIDTH
                } else {
                    n
                };
                self.cursor[0] += if (self.cursor[0] + n) > BUFFER_WIDTH {
                    BUFFER_WIDTH
                } else {
                    n
                };
            }
            // Cursor Backward
            'D' => {
                let mut n = 1;

                for param in params.iter() {
                    n = param[0] as usize;
                }

                self.writer[0] -= if self.writer[0] == 1 { 0 } else { n };
                self.cursor[0] -= if self.cursor[0] == 1 { 0 } else { n };
            }
            // Move cursor to column x
            'G' => {
                let (_, y) = self.cursor_position();
                let mut x = 1;

                for param in params.iter() {
                    x = param[0] as usize; // 1-indexed value
                }

                if x > BUFFER_WIDTH {
                    return;
                }

                self.set_writer_position(x - 1, y);
                self.set_cursor_position(x - 1, y);
            }
            // Move cursor to x;y
            'H' => {
                let mut x = 1;
                let mut y = 1;

                for (i, param) in params.iter().enumerate() {
                    match i {
                        0 => y = param[0] as usize, // 1-indexed value
                        1 => x = param[0] as usize, // 1-indexed value
                        _ => break,
                    };
                }

                if x > BUFFER_WIDTH || y > BUFFER_HEIGHT {
                    return;
                }

                self.set_writer_position(x - 1, y - 1);
                self.set_cursor_position(x - 1, y - 1);
            }
            // Erase in Display
            'J' => {
                let mut n = 0;

                for param in params.iter() {
                    n = param[0] as usize;
                }

                match n {
                    // TODO: 0 and 1, from cursor to begining or to end of screen
                    2 => self.clear_screen(),
                    _ => return,
                }

                self.set_writer_position(0, 0);
                self.set_cursor_position(0, 0);
            }
            // Erase in Line
            'K' => {
                let (x, y) = self.cursor_position();
                let mut n = 0;

                for param in params.iter() {
                    n = param[0] as usize;
                }

                match n {
                    0 => self.clear_row_after(x, y),
                    1 => return, // TODO: self.clear_row_before(x, y),
                    2 => self.clear_row_after(0, y),
                    _ => return,
                }

                self.set_writer_position(x, y);
                self.set_cursor_position(x, y);
            }
            // Enable
            'h' => {
                for param in params.iter() {
                    match param[0] {
                        12 => self.enable_echo(),
                        25 => self.enable_cursor(),
                        _ => return,
                    }
                }
            }
            // Disable
            'l' => {
                for param in params.iter() {
                    match param[0] {
                        12 => self.disable_echo(),
                        25 => self.disable_cursor(),
                        _ => return,
                    }
                }
            }
            _ => {}
        }
    }
}

//==========================
// PRIVATE
//==========================

// 0x00 -> top
// 0x0F -> bottom
// 0x1F -> max (invisible)
fn set_underline_location(location: u8) {
    interrupts::without_interrupts(|| {
        let mut addr: Port<u8> = Port::new(CRTC_ADDR_REG);
        let mut data: Port<u8> = Port::new(CRTC_DATA_REG);

        unsafe {
            addr.write(0x14); // Underline Location Register
            data.write(location);
        }
    })
}

/// Sets the attribute control register
///
/// # Arguments
///
/// * `index` - Index of the attribute
/// * `value` - Value of the attribute
fn set_attr_ctrl_reg(index: u8, value: u8) {
    interrupts::without_interrupts(|| {
        let mut isr: Port<u8> = Port::new(INPUT_STATUS_REG);
        let mut addr: Port<u8> = Port::new(ATTR_ADDR_DATA_REG);

        unsafe {
            isr.read(); // Reset to address mode
            let tmp = addr.read();
            addr.write(index);
            addr.write(value);
            addr.write(tmp);
        }
    })
}

/// Gets the value of the given attribute
///
/// # Arguments
///
/// * `index` - Index of the attribute
fn get_attr_ctrl_reg(index: u8) -> u8 {
    interrupts::without_interrupts(|| {
        let mut isr: Port<u8> = Port::new(INPUT_STATUS_REG);
        let mut addr: Port<u8> = Port::new(ATTR_ADDR_DATA_REG);
        let mut data: Port<u8> = Port::new(ATTR_DATA_READ_REG);
        let index = index | 0x20; // Set "Palette Address Source" bit
        unsafe {
            isr.read(); // Reset to address mode
            let tmp = addr.read();
            addr.write(index);
            let res = data.read();
            addr.write(tmp);
            res
        }
    })
}

//==========================
// PUBLIC
//==========================

lazy_static! {
    pub static ref PARSER: Mutex<Parser> = Mutex::new(Parser::new());
    pub static ref WRITER: Mutex<Writer> = Mutex::new(Writer {
        cursor: [0; 2],
        writer: [0; 2],
        color_code: ColorCode::new(FG, BG),
        buffer: unsafe { &mut *(0xB8000 as *mut Buffer) },
    });
}

#[doc(hidden)]
pub fn print_fmt(args: fmt::Arguments) {
    interrupts::without_interrupts(|| {
        WRITER.lock().write_fmt(args).expect("Could not print to VGA");
    });
}

// Convert 8-bit to 6-bit color
fn vga_color(color: u8) -> u8 { color >> 2 }

/// Gets buffer width
pub fn cols() -> usize { BUFFER_WIDTH }

/// Gets buffer height
pub fn rows() -> usize { BUFFER_HEIGHT }

/// Gets the writer color
pub fn color() -> (Color, Color) { interrupts::without_interrupts(|| WRITER.lock().color()) }

/// Sets the writer color
///
/// # Arguments
///
/// * `foreground` - Foreground color
/// * `background` - Background color
pub fn set_color(foreground: Color, background: Color) {
    interrupts::without_interrupts(|| WRITER.lock().set_color(foreground, background))
}

/// Sets the writer palette
///
/// # Arguments
///
/// * `palette` - New palette
pub fn set_palette(palette: Palette) {
    interrupts::without_interrupts(|| WRITER.lock().set_palette(palette))
}

// ASCII Printable
// Backspace
// New Line
// Carriage Return
// Extended ASCII Printable
pub fn is_printable(c: u8) -> bool { matches!(c, 0x20..=0x7E | 0x08 | 0x0A | 0x0D | 0x7F..=0xFF) }

//==========================
// INIT
//==========================

pub fn init() {
    if !cfg!(feature = "quiet") {
        log!("Initializing VGA... ");
    }

    // Map palette registers to color registers
    set_attr_ctrl_reg(0x0, 0x00);
    set_attr_ctrl_reg(0x1, 0x01);
    set_attr_ctrl_reg(0x2, 0x02);
    set_attr_ctrl_reg(0x3, 0x03);
    set_attr_ctrl_reg(0x4, 0x04);
    set_attr_ctrl_reg(0x5, 0x05);
    set_attr_ctrl_reg(0x6, 0x14);
    set_attr_ctrl_reg(0x7, 0x07);
    set_attr_ctrl_reg(0x8, 0x38);
    set_attr_ctrl_reg(0x9, 0x39);
    set_attr_ctrl_reg(0xA, 0x3A);
    set_attr_ctrl_reg(0xB, 0x3B);
    set_attr_ctrl_reg(0xC, 0x3C);
    set_attr_ctrl_reg(0xD, 0x3D);
    set_attr_ctrl_reg(0xE, 0x3E);
    set_attr_ctrl_reg(0xF, 0x3F);

    set_palette(Palette::default());

    // Disable blinking
    let reg = 0x10; // Attribute Mode Control Register
    let mut attr = get_attr_ctrl_reg(reg);
    attr.set_bit(3, false); // Clear "Blinking Enable" bit
    set_attr_ctrl_reg(reg, attr);

    set_underline_location(0x1F); // Disable underline

    if !cfg!(feature = "quiet") {
        let csi_color = crate::sys::console::Style::color("LightGreen");
        let csi_reset = crate::sys::console::Style::reset();
        println!("{}[ok]{}", csi_color, csi_reset);
    }
}

//==========================
// TESTS
//==========================

#[cfg(test)]
mod tests {
    #[test_case]
    pub fn test_println_simple() {
        for _ in 0..200 {
            println!("test_println_simple output");
        }
    }
}
