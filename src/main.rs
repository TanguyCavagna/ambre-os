//! Custom Rust OS named rust-os following the os.phil-opp.com and inspired by MOROS
//! tutorial.

#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(test_framework::test_runner::test_runner)]
#![reexport_test_harness_main = "test_main"]

extern crate alloc;

use core::panic::PanicInfo;

use bootloader::{entry_point, BootInfo};
use kernel::{println, sys};
// use kernel::task::{executor::Executor, keyboard, Task};

//==========================
// PANIC SETUP
//==========================

/// Runtime panic handler
#[cfg(not(test))]
#[panic_handler]
pub fn panic_handler(info: &PanicInfo) -> ! {
    println!("{}", info);
    utils::hlt_loop();
}

/// Test panic handler
#[cfg(test)]
#[panic_handler]
pub fn panic_handler(info: &PanicInfo) -> ! { test_framework::test_panic_handler(info); }

//==========================
// ENTRY POINT
//==========================

#[cfg(test)]
pub fn kernel_test() { test_main(); }

// pub fn kernel_main() {
//     if !cfg!(feature = "quiet") {
//         println!("Loading Task Register");
//     }

//     println!();

//     let mut executor = Executor::new();
//     executor.spawn(Task::new(keyboard::print_keypresses()));
//     executor.spawn(Task::new(get_datetime()));
//     executor.run();
// }

entry_point!(kernel);
fn kernel(boot_info: &'static BootInfo) -> ! {
    // kernel setup
    kernel::init(boot_info);

    #[cfg(test)]
    kernel_test();

    println!("\x1b[?25h"); // enable cursor

    loop {
        if let Some(cmd) = option_env!("RUST_OS_CMD") {
            println!("{}", cmd);
        } else {
            userspace();
        }
    }
}

fn userspace() {
    let script = "/init/boot.sh";

    if sys::fs::File::open(script).is_some() {
        println!("Opened");
    } else {
        if sys::fs::is_mounted() {
            println!("Could not found '{}'", script);
        } else {
            println!("RFS is not mounted to '/'");
        }

        println!("Running console in diskless mode");
        usr::shell::main();
    }

    utils::hlt_loop();
}

//==========================
// TESTS
//==========================

#[cfg(test)]
mod tests {
    use kernel::println;

    #[test_case]
    pub fn test_println_simple() {
        for _ in 0..200 {
            println!("test_println_simple output");
        }
    }
}
