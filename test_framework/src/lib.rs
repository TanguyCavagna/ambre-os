//! Test framework used throughout the project

#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(test_runner::test_runner)]
#![reexport_test_harness_main = "test_main"]

pub mod ansi_colors;
pub mod qemu_exit;
pub mod test_runner;
pub mod testable;

//==========================
// STD - EXTERNAL - CRATE IMPORTS
//==========================

#[cfg(not(test))]
use core::panic::PanicInfo;
#[cfg(test)]
use core::panic::PanicInfo;

#[cfg(test)]
use bootloader::{entry_point, BootInfo};

//==========================
// PANIC HANDLER
//==========================

/// Test panic handler used everywhere in the project.
///
/// # Arguments
/// * `info` - Panic informations
///
/// # Example
///
/// This very piece of code **must** be present in every workspace which will contains unit test. So pretty much all of theme.
///
/// ```
/// #[cfg(test)]
/// #[panic_handler]
/// pub fn panic(info: &PanicInfo) -> ! { test_framework::test_panic_handler(info); }
/// ```
pub fn test_panic_handler(info: &PanicInfo) -> ! {
    utils::serial_println!("{}", ansi_colors::Red("[PANIC]"));
    utils::serial_println!("Error: {}\n", info);
    qemu_exit::exit_qemu(qemu_exit::QemuExitCode::Failed);

    utils::hlt_loop();
}

/// Apply the custom panic handler
#[cfg(test)]
#[panic_handler]
pub fn panic(info: &PanicInfo) -> ! { test_panic_handler(info); }

//==========================
// ENTRY POINT
//==========================

#[cfg(test)]
entry_point!(test_test_framework_main);

#[cfg(test)]
fn test_test_framework_main(_boot_info: &'static BootInfo) -> ! {
    test_main();
    utils::hlt_loop();
}
