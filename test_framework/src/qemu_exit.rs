//! QEMU exit codes

/// Stores the exits codes for QEMU
///
/// # Safety
///
/// Must set `test-success-exit-code = 33 # (0x10 << 1) | 1` under the `[package.metadata.bootimage]` section in the Cargo.toml file
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u32)]
pub enum QemuExitCode {
    Success = 0x10,
    Failed  = 0x11,
}

/// Exists QEMU with the given exit code
pub fn exit_qemu(exit_code: QemuExitCode) {
    use x86_64::instructions::port::Port;

    unsafe {
        let mut port = Port::new(0xf4);
        port.write(exit_code as u32);
    }
}
