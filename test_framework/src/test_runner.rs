//! Runner for all test found throughout the project

use utils::serial_println;

use crate::{
    ansi_colors::Yellow,
    qemu_exit::{exit_qemu, QemuExitCode},
    testable::Testable,
};

/// Runner which convert all found `test_case` functions to a Testable trait.
///
/// # Output example
///
/// ```txt
/// Running 1 test(s):
/// rust_os::kernel::tests::simple_boot... [ok]
/// ```
pub fn test_runner(tests: &[&dyn Testable]) {
    serial_println!(
        "\n{} {} {}",
        Yellow("Running"),
        tests.len(),
        Yellow("test(s):")
    );

    for test in tests {
        test.run();
    }

    serial_println!();

    exit_qemu(QemuExitCode::Success);
}
