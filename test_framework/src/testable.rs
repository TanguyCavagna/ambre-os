//! Testable trait for all `test_case` found.

use crate::ansi_colors::Green;

pub trait Testable {
    fn run(&self);
}

/// Must be implemented for a function
impl<T> Testable for T
where
    T: Fn(),
{
    fn run(&self) {
        utils::serial_print!("{}...\t", core::any::type_name::<T>());
        self();
        utils::serial_println!("{}", Green("[ok]"));
    }
}
