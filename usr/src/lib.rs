#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(test_framework::test_runner::test_runner)]
#![reexport_test_harness_main = "test_main"]

#[cfg(test)]
use core::panic::PanicInfo;

#[cfg(test)]
#[panic_handler]
pub fn panic_handler(info: &PanicInfo) -> ! { test_framework::test_panic_handler(info); }

pub mod shell;
