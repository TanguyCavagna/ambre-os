use kernel::{print, println, sys};

pub fn main() {
    println!("Hello from usr !");
    println!();

    let csi_color = sys::console::Style::color("Magenta");
    let csi_reset = sys::console::Style::reset();
    print!("{}>{} ", csi_color, csi_reset);

    utils::hlt_loop();
}
