//! Utils workspace where macros, constants, and other informations
//!
//! # Safety
//!
//! The `utils` workspace **cannot** be tested as the other workspaces because it cannot have any local
//! crate dependencies due to its numerous import in pretty much all the workspaces.

#![no_std]
#![no_main]

pub mod serial;

//==========================
//      HALT INST
//==========================

/// Prevent the CPU to run at full speed when doing nothing
pub fn hlt_loop() -> ! {
    loop {
        x86_64::instructions::hlt();
    }
}
