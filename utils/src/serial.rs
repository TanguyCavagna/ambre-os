//! Serial communication module. Initialy used to send tests output from the kernel to the console
//! stdout.

use lazy_static::lazy_static;
use spin::Mutex;
use uart_16550::SerialPort;

lazy_static! {
    /// Setup serial port 1
    pub static ref SERIAL1: Mutex<SerialPort> = {
        let mut serial_port = unsafe { SerialPort::new(0x3F8) }; // Standard serial interface port
        serial_port.init();
        Mutex::new(serial_port)
    };
}

//==========================
// MACROS
//==========================

#[doc(hidden)]
pub fn print_fmt(args: core::fmt::Arguments) {
    use core::fmt::Write;

    use x86_64::instructions::interrupts;

    interrupts::without_interrupts(|| {
        SERIAL1.lock().write_fmt(args).expect("Printing to serial failed");
    })
}

/// Prints to the host through the serial port
#[macro_export]
macro_rules! serial_print {
    ($($arg:tt)*) => {
        $crate::serial::print_fmt(format_args!($($arg)*));
    };
}

/// Prints to the host through the serial interface, appending a new line
#[macro_export]
macro_rules! serial_println {
    () => ($crate::serial_print!("\n"));
    ($fmt:expr) => ($crate::serial_print!(concat!($fmt, "\n")));
    ($fmt:expr, $($arg:tt)*) => ($crate::serial_print!(concat!($fmt, "\n"), $($arg)*));
}

//==========================
// TESTS
//==========================
